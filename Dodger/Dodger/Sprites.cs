﻿using System;

// Custom Library
using Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Dodger {

    public class Sprites {

        #region Variables

        public float
            mObject = 60, // Speed of object
            sMove = 0; // Current object's speed

        public int type; // Texture Type
        private Random rand = new Random();
        private float sMulti = 1;

        public Vector2 pos = Vector2.Zero; // Set Vector to (0, 0)

        #endregion Variables

        /// <summary>
        /// Drawing the sprite
        /// <summary>
        /// <param name="sBatch">SpriteBatch</param>
        public void Draw( SpriteBatch sBatch ) {
            if( mObject != GamePlay.mObject ) {
                sMove = mObject - ( float )((mObject - GamePlay.mObject) / 1.1); // Change the Speed
            } else
                sMove = mObject;
            if( !Main.pause )
                pos -= new Vector2(sMove * sMulti * 13, 0) * ( float )Main.gTime; // Move the Object
            bool draw = true; // Can Draw?
            if( Main.hidden && pos.X > 775 ) // If Hidden is ON
                draw = false;
            else if( Main.rHidden && pos.X <= 600 ) // If rHidden is ON
                draw = false;
            if( draw ) {
                if( type == 2 )
                    sBatch.Draw(Main.wall[Main.iWall], pos, Color.White); // Draw the sprite Wall
                else if( type == 3 )
                    sBatch.Draw(Main.circle[Main.iCircle], pos, Color.White); // Draw the sprite Object
            }
        }

        /// <summary>
        /// Load Content for Sprite Class
        /// </summary>
        /// <param name="type">Type of Object</param>
        public void LoadContent( int type, int mObject ) {
            try {
                this.mObject = mObject;
                if( type == 1 )
                    this.type = rand.Shuffle(2, 3); // Random Wall or Circle Respectively
                else
                    this.type = type;
            } catch( ContentLoadException ) { }
        }
    }
}