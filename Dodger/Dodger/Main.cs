using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;

// Custom Library
using Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Dodger {

    /// <summary>
    /// This class will content nothing other then the main menu layout
    /// for the game. All other Classes will call on this Class Variables
    ///
    /// @author Huu Vu
    /// @Version 1.5
    /// </summary>
    public class Main: Microsoft.Xna.Framework.Game {
        /*
        * This is the most most complex variable in the game.
        * It determine the current progress of the game
        * and what should be loaded/draw
        * Influenced by the IBM Master the MainFrame
        *
        * -1: Game End
        *
        * 0: Main Menu of the game
        *  1: Instruction
        *   1: Skins Option
        *  2: Game Mode
        *
        * 1: Play Beta Mode
        *  1:
        *   0:User Input Game Spawner Setting
        *   1: user Input Game Object Speed Setting
        *   2: User Input What Object to Spawn
        *   3: User Input of Multi Spawn
        *   4: User Input of Speedy Spawn
        *  2: User Dead in Beta Mode
        *
        * 2: Play Normal Mode
        *  1: Game User Input Game Object
        *  2: User Dead in Normal Mode
        *
        * 3: Play Musical Mode
        *  1: Choose Song?
        *  2: User Dead
        */
        public static double menu = 0.0;

        /*
         * Main Menu
         * 0: Start
         * 1: Instruction
         * 2: Quit
         *
         * Game Mode
         * 3: Beta Mode
         * 4: Normal Mode
         * 5: Musical Mode
         */
        public static int option = 0;

        #region Variables

        #region Public Static

        public static string
            cDir, // Current Directory
            sConvert = ""; // Current Song Convertion Process

        public static Thread cSong; // Convert Song

        public static int
            mScroll, // Scroll Amount
            iStext = 0, // Circle
            iWall = 0, // Wall
            iCircle = 0, // Object
            iError = 0; // Error

        public static double
            score, // Offical Score
            sUpdate; // Score To display

        public static bool
            pause = false, // Pause Screen
            bKstate = true, // Keyboard State
            bMstate = false, // Mouse State
            Exiting = false, // Is the Program Exiting?
            picture = true, // Allow Picture
            check = true, // Check Collison
            aPause = true, // Allow Pausing
            easter = false, // Easter Egg
            hidden = false, // Hidden
            rHidden = false; // Reverse Hidden

        public static float gTime; // Game time
        public static List<Sprites> sObject; // Sprite Class

        #endregion Public Static

        #region Privates

        private Process pLame = new Process(); // Load up lame.exe encoder

        private string
            SkinPath = "", // Path of Skin
            command = "", // Option Command
            mode = ""; // Hidden/rHidden

        private int index = 0; // Index of Skin
        private List<string> errors; // Errors

        private bool
            dance = true, // Show Visualizer
            debug = false; // Allow Debug Option

        private List<string> sPath; // All the Skins Folders

        private float rot = 0f, // Rotation of Arrow
            mAlpha = 1f, // Main Menu Alpha
            gAlpha = 0f, // Game Mode Alpha
            iAlpha = 0f, // Instruction Alpha
            bAlpha = .3f; // Picuter Alpha

        #endregion Privates

        #region XNA's

        #region Public Static

        public static Texture2D sLay; // Music Song Selection

        public static List<Texture2D>
           sText, // Player
           wall, // Wall
           circle; // Circle

        public static SoundEffect back, // Back Effect
            hit, // Hit Effect
            tick, // Tick Effect
            bounce, // Jump!
            bound; // Floor/Wall Bounce

        public static Vector2 size; // Size of the Screen

        #endregion Public Static

        #region Privates

        private Vector2
            pos, // Arrow Position
            move, // Move Amount for Arrow
            instruct = Vector2.Zero, // Instruction Location
            iMove = Vector2.Zero; // Instruction Move Amount

        private Texture2D
           sMenu, // Main Menu
           sMode, // Mode Screen
           sInstruct, // Instruction Screen
           sPause, // Pause Menu
           aText, // Arrow
           black; // Black Screen

        #endregion Privates

        public SpriteFont
            font, // For Normal Usages
            fSong, // For Song Titles
            fMenu; // For Display Messages

        private KeyboardState kState;
        private MouseState mState;

        private GraphicsDeviceManager g;
        private SpriteBatch s;

        #endregion XNA's

        #region Class

        private GamePlay p; // Gameplay Class (Play the Game)
        private Music m; // Music Class (Control Music and Visuals)

        #endregion Class

        #endregion Variables

        #region XNA Methods

        /// <summary>
        /// Useless
        /// </summary>
        public Main() {
            Content.RootDirectory = "Content"; // Get Ready
            this.Window.Title = "Dodger"; // Name of Game

            IsFixedTimeStep = true; // Get Set
            TargetElapsedTime = new TimeSpan(0, 0, 0, 0, 20); // Limit!

            g = new GraphicsDeviceManager(this); // Set Graphics Device
            g.SynchronizeWithVerticalRetrace = true; // No Vertical Sync
            g.PreferredBackBufferWidth = 1280; // Set size
            g.PreferredBackBufferHeight = 950; // Set Height
            g.PreferMultiSampling = true; // Smooth Images
            g.ApplyChanges(); // Set Changes

            // Create Directories
            Directory.CreateDirectory(".\\Music");
            Directory.CreateDirectory(".\\Skin");
            Directory.CreateDirectory(".\\Music\\Music Media");
            Directory.CreateDirectory(".\\Music\\Music");
            cDir = Directory.GetCurrentDirectory();
        }

        /// <summary>
        /// Initialized the Game and Graphics
        /// </summary>
        protected override void Initialize() {

            #region Skin Instruction Text

            if( !File.Exists(".\\Skin\\Instruction.txt") ) { // If Instuction file does not exist
                using( StreamWriter sr = File.CreateText(".\\Skin\\Instruction.txt") ) { // Create a new File with the following text
                    sr.WriteLine("The following are file that Dodger can read and load: ");
                    sr.WriteLine("For Sprites:");
                    sr.WriteLine("Circle.png - This is for the Player Circle.");
                    sr.WriteLine("Wall.png - This is for the Wall Object.");
                    sr.WriteLine("Object.png - This is for the Small Circle Object.");
                    sr.WriteLine("All of these picture file be animated by create multiple file with the same name along with a (*number*)");
                    sr.WriteLine("Ex: Circle.png\nCirlce (1).png\nCirlce (2).png...");
                    sr.WriteLine("For Sound Effects:");
                    sr.WriteLine("Back.wav - This is for the Back SoundEffect.");
                    sr.WriteLine("Hit.wav - This is for the Hit SoundEffect.");
                    sr.WriteLine("Tick.wav - This is for the Tick SoundEffect (Increasing/Decreasing Values).");
                    sr.WriteLine("Bounce.wav - This is for the Bouncing SoundEffect.");
                    sr.WriteLine("Bound.wav - This is for the Boundaries SoundEffect (When the Player Piece hits the top or bottom).");
                }
            }

            #endregion Skin Instruction Text

            #region Read/Write Config

            if( !File.Exists(cDir + "\\Config.db") ) { // If Config File Does NOT Exists

                #region Create Data

                using( StreamWriter sr = File.CreateText(cDir + "\\Config.db") ) { // Create a new File
                    sr.WriteLine("Skin Path:Default"); // Set with Default Skin
                    sr.WriteLine("Sound:.25"); // Default Sound Volume
                    sr.WriteLine("Music:1"); // Default Music Volume
                    sr.WriteLine("Dance:true"); // Dance?
                    sr.WriteLine("Picture:true"); // Picture?
                    sr.WriteLine("Fullscreen:false"); // Fullscreen?
                }
                // Set Default
                SkinPath = "Default";
                SoundEffect.MasterVolume = .25f;
                MediaPlayer.Volume = 1f;
                dance = true;
                picture = true;

                #endregion Create Data
            } else { // If File DOES Exist

                #region Load Data

                using( StreamReader sr = File.OpenText(cDir + "\\Config.db") ) { // Load the File into the Stream
                    string s; // Temp String
                    while( (s = sr.ReadLine()) != null ) { // Read Each Line
                        if( s.Contains("Skin Path:") ) { // If line contains the skin path
                            if( Directory.Exists(".\\Skin\\" + s.Remove(0, s.IndexOf(":") + 1)) )
                                SkinPath = s.Remove(0, s.IndexOf(":") + 1); // Get the location
                            else
                                SkinPath = "Default"; // If Skin not there
                        }
                        if( s.Contains("Sound:") ) // Sound Volume
                            SoundEffect.MasterVolume = ( float )Convert.ToDouble(s.Remove(0, s.IndexOf(":") + 1));
                        if( s.Contains("Music:") ) // Music Volume
                            MediaPlayer.Volume = ( float )Convert.ToDouble(s.Remove(0, s.IndexOf(":") + 1));
                        if( s.Contains("Dance:") ) { // Dance
                            if( s.Remove(0, s.IndexOf(":") + 1).Equals("true") )
                                dance = true;
                            else if( s.Remove(0, s.IndexOf(":") + 1).Equals("false") )
                                dance = false;
                        }
                        if( s.Contains("Picture:") ) { // Picture
                            if( s.Remove(0, s.IndexOf(":") + 1).Equals("true") )
                                picture = true;
                            else if( s.Remove(0, s.IndexOf(":") + 1).Equals("false") )
                                picture = false;
                        }
                        if( s.Contains("Fullscreen:") ) { // Fullscreen
                            if( s.Remove(0, s.IndexOf(":") + 1).Equals("true") )
                                g.IsFullScreen = true;
                            else if( s.Remove(0, s.IndexOf(":") + 1).Equals("false") )
                                g.IsFullScreen = false;

                            g.ApplyChanges();
                        }
                    }

                #endregion Load Data
                }
            }

            #endregion Read/Write Config

            #region Define Variables

            pos = new Vector2(725, 660); // Arrow Position
            sObject = new List<Sprites>(); // New List of Sprites
            sPath = new List<string>(); // New List of Skin Path
            m = new Music(); // New Music Class

            #endregion Define Variables

            #region Skin Variables

            sPath.Add("Default"); // Add Default Skin to Path
            foreach( string s in Directory.GetDirectories(".\\Skin\\") ) {
                sPath.Add(s.Remove(0, s.LastIndexOf("\\") + 1)); // Add all Skin Folder to Skin Path
            }
            if( SkinPath.Equals("Default") )
                index = 0; // If the Default Skin is Chosen
            else if( sPath.Contains(SkinPath.Remove(SkinPath.LastIndexOf("\\"))) )
                index = Math.Abs(sPath.IndexOf(SkinPath.Remove(SkinPath.LastIndexOf("\\")))); // Find Index of Current Skin Path

            #endregion Skin Variables

            base.Initialize();
        }

        /// <summary>
        /// Load
        /// </summary>
        protected override void LoadContent() {
            // Create a new SpriteBatch, which can be used to draw textures.
            s = new SpriteBatch(GraphicsDevice);
            // Start Converter Thread to start converting song
            LoadSkin(); // Load the Rest of the Contents

            #region Load Defaults

            // Textures
            aText = Content.Load<Texture2D>("Image\\Arrow"); // Arrow for Main Menu
            sMenu = Content.Load<Texture2D>("Image\\Main"); // Main Menu
            sMode = Content.Load<Texture2D>("Image\\Mode"); // Game Mode
            sPause = Content.Load<Texture2D>("Image\\Pause"); // Pause Menu
            sInstruct = Content.Load<Texture2D>("Image\\Instruction"); // Main Menu
            sLay = Content.Load<Texture2D>("Image\\Layout"); // Musical Layout
            black = Content.Load<Texture2D>("Image\\Pix"); // Pix Image (Reduce over head)
            // Font
            font = Content.Load<SpriteFont>("Font");
            fSong = Content.Load<SpriteFont>("Song");
            fMenu = Content.Load<SpriteFont>("Text");

            #endregion Load Defaults

            #region Add Songs

            // Reflector (Make Internal Constructor of Song)
            var ctor = typeof(Song).GetConstructor(BindingFlags.NonPublic | BindingFlags.Instance, null, new[] { typeof(string), typeof(string), typeof(int) }, null);
            if( !File.Exists(cDir+ "\\Songs.db") )
                File.CreateText(cDir + "\\Songs.db"); // Create Database
            List<string> temp = File.ReadAllLines(cDir + "\\Songs.db").ToList<string>(); // Read Database

            foreach( string st in temp ) { // For all the song name
                if( File.Exists(st) ) // If it exist
                {
                    string title = st.Remove(0, st.LastIndexOf("\\") + 1); // get Title of Song
                    Music.songs.Add(new Music.SongsList { sSong = ( Song )ctor.Invoke(new object[] { title.Remove(title.LastIndexOf(".")), st, 0 }) }); // Add Song by using the internal constructor
                    Music.songs.Shuffle(); // Mix it Up
                }
            }

            for( int x = Music.songs.Count - 1; x >= 0; x-- ) { // Try to find image for each song
                List<string> files = Directory.GetFiles(cDir +"\\Music\\Music Media", Music.songs[x].sSong.Name).ToList(); // Try to find Picture
                if( files.Count() > 0 ) { // If a Picture is found
                    FileStream f = new FileStream(files[0], FileMode.Open); // Open Picture in a Stream
                    Music.songs[x].sPict = Texture2D.FromStream(GraphicsDevice, f); // Add Picture
                    f.Close(); // Close Stream
                }
            }
            cSong = new Thread(ConvertFile);
            cSong.Start();

            #endregion Add Songs

            size = new Vector2(GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height - sText[iStext].Height - 100); // Screen size (for Jump)
            p = new GamePlay();
        }

        /// <summary>
        /// Unload
        /// </summary>
        protected override void UnloadContent() {
        }

        #endregion XNA Methods

        #region Extra Method

        /// <summary>
        /// Load Custom Skin Elements
        /// </summary>
        public void LoadSkin() {
            string temp = ".\\Skin\\" + SkinPath; // Path of the Skin
            Texture2D tTemp; // Temp Texture to Convert

            #region Create New Texture

            sText = new List<Texture2D>();
            wall = new List<Texture2D>();
            circle = new List<Texture2D>();

            #endregion Create New Texture

            errors = new List<string>(); // Errors??
            GraphicsDevice.Clear(Color.White); // Clear Graphic Buffer
            if( Directory.Exists(temp) ) { // If Skin Directory Exist
                FileStream f; // Create a new Stream
                Process pLame = new Process(); // To Convert any Sound File

                #region Sound Effect

                // Pattern is the same throughout
                if( File.Exists(temp + "Back.wav") ) { // If there is a custom sound file
                    f = new FileStream(temp + "Back.wav", FileMode.Open); // Open File
                    try {
                        back = SoundEffect.FromStream(f); // Add Sound
                    } catch( ArgumentException e ) { // If Sound Cannot Load
                        errors.Add("Error Loading Back.wav. Error: " + e); // Add the error
                        back = Content.Load<SoundEffect>("Sound\\Back"); // Use Default
                    }
                    f.Close(); // Close Stream
                } else // If there is No Sound File
                    back = Content.Load<SoundEffect>("Sound\\Back");

                if( File.Exists(temp + "Hit.wav") ) {
                    f = new FileStream(temp + "Hit.wav", FileMode.Open);
                    try {
                        hit = SoundEffect.FromStream(f);
                    } catch( ArgumentException e ) {
                        errors.Add("Error Loading Hit.wav. Error: " + e);
                        hit = Content.Load<SoundEffect>("Sound\\Hit");
                    }
                    f.Close();
                } else
                    hit = Content.Load<SoundEffect>("Sound\\Hit");

                if( File.Exists(temp + "Tick.wav") ) {
                    f = new FileStream(temp + "Tick.wav", FileMode.Open);
                    try {
                        tick = SoundEffect.FromStream(f);
                    } catch( ArgumentException e ) {
                        errors.Add("Error Loading Tick.wav. Error: " + e);
                        tick = Content.Load<SoundEffect>("Sound\\Tick");
                    }
                    f.Close();
                } else
                    tick = Content.Load<SoundEffect>("Sound\\Tick");

                if( File.Exists(temp + "Bounce.wav") ) {
                    f = new FileStream(temp + "Bounce.wav", FileMode.Open);
                    try {
                        bounce = SoundEffect.FromStream(f);
                    } catch( ArgumentException e ) {
                        errors.Add("Error Loading Bounce.wav. Error: " + e);
                        bounce = Content.Load<SoundEffect>("Sound\\Bounce");
                    }
                    f.Close();
                } else
                    bounce = Content.Load<SoundEffect>("Sound\\Bounce");

                if( File.Exists(temp + "Bound.wav") ) {
                    f = new FileStream(temp + "Bound.wav", FileMode.Open);
                    try {
                        bound = SoundEffect.FromStream(f);
                    } catch( ArgumentException e ) {
                        errors.Add("Error Loading Bound.wav. Error: " + e);
                        bound = Content.Load<SoundEffect>("Sound\\Bound");
                    }
                    f.Close();
                } else
                    bound = Content.Load<SoundEffect>("Sound\\Bound");

                #endregion Sound Effect

                #region Texture

                // Pattern is the same throughout
                for( int x = 0; x <= Directory.GetFiles(temp, "Circle*").Count() - 1; x++ ) { // If there is a Circle Picture
                    if( x == 0 ) { // If the File is without number (first picture in animation)
                        tTemp = Texture2D.FromStream(GraphicsDevice, (f = new FileStream(temp + "Circle.png", FileMode.Open))); // Test Image
                        f.Close(); // Close Stream
                        if( tTemp.Width != 75 && tTemp.Height != 75 ) { // If Image is too big/small, recreate image
                            tTemp.SaveAsPng((f = new FileStream(temp + "Circle.png", FileMode.Create)), 75, 75); // Recreate image with certain size
                            f.Close(); // Close (Close to load up new Image)
                            tTemp = Texture2D.FromStream(GraphicsDevice, (f = new FileStream(temp + "Circle.png", FileMode.Open)), 75, 75, true); // Add new image
                            f.Close(); // Close
                        }
                        sText.Add(tTemp); // Add Image
                        f.Close(); // Close
                    } else if( File.Exists(temp + "Circle (" + x + ").png") ) { // If file is number (animation)
                        tTemp = Texture2D.FromStream(GraphicsDevice, (f = new FileStream(temp + "Circle (" + x + ").png", FileMode.Open))); // Test Image
                        f.Close(); // Close Stream
                        if( tTemp.Width != 75 && tTemp.Height != 75 ) { // If Image is too big/small, recreate image
                            tTemp.SaveAsPng((f = new FileStream(temp + "Circle (" + x + ").png", FileMode.Create)), 75, 75); // Recreate image with certain size
                            f.Close();// Close (Close to load up new Image)
                            tTemp = Texture2D.FromStream(GraphicsDevice, (f = new FileStream(temp + "Circle (" + x + ").png", FileMode.Open)), 75, 75, true);// Add new image
                            f.Close();// Close
                        }
                        sText.Add(tTemp); // Add Image
                        f.Close(); // Close
                    }
                }
                for( int x = 0; x <= Directory.GetFiles(temp, "Wall*").Count() - 1; x++ ) {
                    if( x == 0 ) {
                        tTemp = Texture2D.FromStream(GraphicsDevice, (f = new FileStream(temp + "Wall.png", FileMode.Open)));// Test Image
                        f.Close();
                        if( tTemp.Width != 65 && tTemp.Height != 285 ) { // If Image is too big/small, recreate image
                            tTemp.SaveAsPng((f = new FileStream(temp + "Wall.png", FileMode.Create)), 65, 285);
                            f.Close();
                            tTemp = Texture2D.FromStream(GraphicsDevice, (f = new FileStream(temp + "Wall.png", FileMode.Open)), 65, 285, true);
                            f.Close();
                        }
                        wall.Add(tTemp);
                        f.Close();
                    } else if( File.Exists(temp + "Wall (" + x + ").png") ) {
                        tTemp = Texture2D.FromStream(GraphicsDevice, (f = new FileStream(temp + "Wall (" + x + ").png", FileMode.Open)));// Test Image
                        f.Close();
                        if( tTemp.Width != 65 && tTemp.Height != 285 ) { // If Image is too big/small, recreateimage
                            tTemp.SaveAsPng((f = new FileStream(temp + "Wall (" + x + ").png", FileMode.Create)), 65, 285);
                            f.Close();
                            tTemp = Texture2D.FromStream(GraphicsDevice, (f = new FileStream(temp + "Wall (" + x + ").png", FileMode.Open)), 65, 285, true);
                            f.Close();
                        }
                        wall.Add(tTemp);
                        f.Close();
                    }
                }
                for( int x = 0; x <= Directory.GetFiles(temp, "Object*").Count() - 1; x++ ) {
                    if( x == 0 ) {
                        tTemp = Texture2D.FromStream(GraphicsDevice, (f = new FileStream(temp + "Object.png", FileMode.Open)));// Test Image
                        f.Close();
                        if( tTemp.Width != 45 && tTemp.Height != 45 ) { // If Image is too big/small, recreateimage
                            tTemp.SaveAsPng((f = new FileStream(temp + "Object.png", FileMode.Create)), 45, 45);
                            f.Close();
                            tTemp = Texture2D.FromStream(GraphicsDevice, (f = new FileStream(temp + "Object.png", FileMode.Open)), 45, 45, true);
                            f.Close();
                        }
                        circle.Add(tTemp);
                        f.Close();
                    } else if( File.Exists(temp + "Object (" + x + ").png") ) {
                        tTemp = Texture2D.FromStream(GraphicsDevice, (f = new FileStream(temp + "Object (" + x + ").png", FileMode.Open)));// Test Image
                        f.Close();
                        if( tTemp.Width != 45 && tTemp.Height != 45 ) { // If Image is too big/small, recreateimage
                            tTemp.SaveAsPng((f = new FileStream(temp + "Object (" + x + ").png", FileMode.Create)), 45, 45);
                            f.Close();
                            tTemp = Texture2D.FromStream(GraphicsDevice, (f = new FileStream(temp + "Object (" + x + ").png", FileMode.Open)), 45, 45, true);
                            f.Close();
                        }
                        circle.Add(tTemp);
                        f.Close();
                    }
                }

                #endregion Texture
            }

            #region Default Skin

            // Sound Effects ( If default skin is to be used)
            if( SkinPath.Equals("Default") )
                back = Content.Load<SoundEffect>("Sound\\Back");
            if( SkinPath.Equals("Default") )
                hit = Content.Load<SoundEffect>("Sound\\Hit");
            if( SkinPath.Equals("Default") )
                tick = Content.Load<SoundEffect>("Sound\\Tick");
            if( SkinPath.Equals("Default") )
                bounce = Content.Load<SoundEffect>("Sound\\Bounce");
            if( SkinPath.Equals("Default") )
                bound = Content.Load<SoundEffect>("Sound\\Bound");
            // Texture ( If default skin is to be used)
            if( sText.Count == 0 || SkinPath.Equals("Default") )
                sText.Add(Content.Load<Texture2D>("Image\\Circle"));
            if( wall.Count == 0 || SkinPath.Equals("Default") )
                wall.Add(Content.Load<Texture2D>("Image\\Wall"));
            if( circle.Count == 0 || SkinPath.Equals("Default") )
                circle.Add(Content.Load<Texture2D>("Image\\Object"));

            #endregion Default Skin
        }

        /// <summary>
        /// This Method Converts Songs into wav and then into mp3 with lame_enc.dll
        /// </summary>
        public void ConvertFile() {
            cSong.Priority = ThreadPriority.Lowest; // Set thread to lowest (So it won't lag the game)

            foreach( string sSong in Directory.GetFiles(cDir + "\\Music", "*") ) { // Get all the song in Music Folder
                if( Exiting )
                    return;
                string title = sSong.Remove(0, sSong.LastIndexOf("\\") + 1); // Title of song
                // sConvert Explains The Process
                if (title.Contains(".mp3") || title.Contains(".wav"))
                { // If the file is a readable file by lame.exe
                    sConvert = "Starting Convertion Thread...";

                    #region Convertion

                    pLame.StartInfo.FileName = cDir + "\\lame.exe"; // Set Directory
                    pLame.StartInfo.UseShellExecute = false; // Should Windows start it?
                    pLame.StartInfo.CreateNoWindow = true; // Should there be NO window?
                    sConvert = title + " --> Decoding";
                    pLame.StartInfo.Arguments = String.Format("--decode \"{0}\" \"{1}\"", sSong, cDir + "\\Music\\Temp"); // Set the parameter ("--decode" turn the file into a wave)
                    pLame.Start(); // Start decoding
                    pLame.WaitForExit(); // Wait for lame.exe to finish
                    sConvert = title + " --> Recoding";
                    pLame.StartInfo.Arguments = String.Format("-V2 -V0 \"{0}\" \"{1}\"", cDir + "\\Music\\Temp", cDir + "\\Music\\Music\\" + title); // Set parameter ("-V2" convert it to mp3 at 202 bit)
                    pLame.Start(); // Set encoding
                    pLame.WaitForExit(); // Wait
                    sConvert = "Cleaning Up";
                    pLame.Close(); // Free Reasources
                    File.Delete(sSong); // Delete the original song file (so lame.exe won't convert it again)
                    File.Delete(cDir + "\\Music\\Temp"); // Delete the temp file
                    sConvert = "Adding " + title + " to song list";
                    
                    #region Add Song

                    var ctor = typeof(Song).GetConstructor(BindingFlags.NonPublic | BindingFlags.Instance, null, new[] { typeof(string), typeof(string), typeof(int) }, null); // Internal Constructor (Reflector)
                    Music.songs.Add(new Music.SongsList { sSong = (Song)ctor.Invoke(new object[] { title.Remove(title.LastIndexOf(".")), cDir + "\\Music\\Music\\" + title, 0 }) }); // Add song
                    sConvert = "Finding " + title + "'s picture.";
                    
                    List<string> files = Directory.GetFiles(".\\Music\\Music Media", title.Remove(title.LastIndexOf(".")) + "*").ToList(); // Find Picture File
                    if( files.Count() > 0 && GraphicsDevice != null ) { // If there is a picture file
                        sConvert = "Adding " + title + "'s picture to song list";
                        FileStream f = new FileStream(files[0], FileMode.Open); // Open Stream
                        Music.songs[Music.songs.Count - 1].sPict = Texture2D.FromStream(GraphicsDevice, f); // Add picture
                        f.Close(); // Close stream
                    } else // No picture :(
                        sConvert = "No " + title + "'s picture.";
  
                    sConvert = "Finished with " + title;

                    #endregion Add Song

                    Thread.Sleep(750); // Wait a bit
                    sConvert = "";

                    #endregion Convertion
                }
            }

            #region Check Song List

            Thread songlist = new Thread(SongList); // Set the song checking process (More explain below)
            songlist.Priority = ThreadPriority.Lowest; // Set thread to lowest also

            try {
                if( !songlist.IsAlive ) { // If songList is not alive
                    songlist = new Thread(SongList); // Set new
                    songlist.Start(); // Start
                }
            } catch( NullReferenceException ) { // If it's null for some reason
                songlist = new Thread(SongList); // Set new
            }

            #endregion Check Song List
        }

        /// <summary>
        /// Remove any song not in Music Directory
        /// This is to make sure there is no empty song in the song list
        /// </summary>
        public void SongList() {

            List<string> sName = new List<string>(); // List name of all songs
            for( int x = Music.songs.Count - 1; x >= 0; x-- )
                sName.Add(Music.songs[x].sSong.Name); // Get name of all songs
             List<Music.SongsList> SongList = Music.songs; // Get the current list of songs

            try {
                for( int x = sName.Count - 1; x >= 0; x-- ) {// Go through the song list
                    if( !File.Exists(sName[x]) ) // If the song does NOT exist
                        Music.songs.RemoveAt(Music.songs.IndexOf(new Music.SongsList { sSong = SongList[x].sSong })); // Remove the song from the song list
                }

                sName.AddRange(Directory.GetFiles(cDir + "\\Music\\")); // Don't remove converting files
                List<string> iDir; // Directory Info

                iDir = Directory.GetFiles(cDir + "\\Music\\Music Media\\").ToList<string>(); // List of all image files
                for( int x = iDir.Count - 1; x >= 0; x-- ) {// Goes through image files
                    Directory.CreateDirectory(cDir + "\\Music\\Unused Files\\");
                    if( !sName.Contains(iDir[x].Remove(0, iDir[x].LastIndexOf("\\") + 1)) ) {// If the file is an oddie then move it
                        if( File.Exists(cDir + "\\Music\\Unused Files\\" + iDir[x].Remove(0, iDir[x].LastIndexOf("\\") + 1)) ) {
                            int z = 1;
                            while( File.Exists(cDir + "\\Music\\Unused Files\\" + iDir[x].Remove(0, iDir[x].LastIndexOf("\\") + 1) + z) )
                                x++;
                            File.Move(iDir[x], cDir + "\\Music\\Unused Files\\" + iDir[x].Remove(0, iDir[x].LastIndexOf("\\") + 1) + z);
                        } else
                            File.Move(iDir[x], cDir + "\\Music\\Unused Files\\" + iDir[x].Remove(0, iDir[x].LastIndexOf("\\") + 1));
                    }
                }
                iDir = Directory.GetFiles(cDir + "\\Music\\Music\\").ToList<string>(); // List of all song files
                for( int x = iDir.Count - 1; x >= 0; x-- ) {// Goes through song files
                    Directory.CreateDirectory(cDir + "\\Music\\Unused Files\\");
                    if( !sName.Contains(iDir[x].Remove(0, iDir[x].LastIndexOf("\\") + 1).Remove(iDir[x].LastIndexOf(".") + 1)) ) { // If the file is an oddie then move it
                        if( File.Exists(cDir + "\\Music\\Unused Files\\" + iDir[x].Remove(0, iDir[x].LastIndexOf("\\") + 1)) ) {
                            int z = 1;
                            while( File.Exists(cDir + "\\Music\\Unused Files\\" + iDir[x].Remove(0, iDir[x].LastIndexOf("\\") + 1) + z) )
                                x++;
                            File.Move(iDir[x], cDir + "\\Music\\Unused Files\\" + iDir[x].Remove(0, iDir[x].LastIndexOf("\\") + 1) + z);
                        } else
                            File.Move(iDir[x], cDir + "\\Music\\Unused Files\\" + iDir[x].Remove(0, iDir[x].LastIndexOf("\\") + 1));
                    }
                }
            } catch( ArgumentOutOfRangeException ) { }
        }

        #endregion Extra Method

        #region Update/Draw

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update( GameTime gameTime ) {

            #region Convertion

            if( !cSong.IsAlive && !Exiting && Directory.GetFiles(cDir + "\\Music\\").Count() > 0 ) {
                cSong = new Thread(ConvertFile);
                cSong.Start();
            }

            #endregion Convertion

            #region Update States

            // Update Keyboard and Mouse States
            kState = Keyboard.GetState();
            mState = Mouse.GetState();
            // Update the Keyboard, Mouse, and Scroll Value
            mScroll = mState.CheckScroll();
            bKstate = kState.CheckPress();
            bMstate = mState.CheckPress();

            p.cColor(); // Changing background color
            menu = Math.Round(menu, 2); // Round off the menu number (for some reason, it would get really long decimal)
            gTime = ( float )gameTime.ElapsedGameTime.TotalSeconds; // Game Time

            #endregion Update States

            #region Music/Sound Volume

            if( menu != 1 && menu != 2 && menu != 3 && menu != 3.1 || pause ) {
                if( Keyboard.GetState().GetPressedKeys().Contains(Keys.LeftShift) || Keyboard.GetState().GetPressedKeys().Contains(Keys.LeftControl) ) {
                    float
                        sVolume = SoundEffect.MasterVolume, // Sound Effect Volume Float
                        mVolume = MediaPlayer.Volume; // Music Volume Float

                    #region Changing Values

                    if( Keyboard.GetState().GetPressedKeys().Contains(Keys.LeftShift) ) {  // Change Sound Effect Volume
                        if( Main.mScroll > 0 || Keyboard.GetState().IsKeyDown(Keys.Up) )
                            sVolume += 0.01f; // Increase
                        else if( Main.mScroll < 0 || Keyboard.GetState().IsKeyDown(Keys.Down) )
                            sVolume -= 0.01f; // Decrease
                    }
                    if( Keyboard.GetState().GetPressedKeys().Contains(Keys.LeftControl) ) { // Change Music Volume
                        if( Main.mScroll > 0 || Keyboard.GetState().IsKeyDown(Keys.Up) )
                            mVolume += 0.01f; // Increase
                        else if( Main.mScroll < 0 || Keyboard.GetState().IsKeyDown(Keys.Down) )
                            mVolume -= 0.01f; // Decrease
                    }

                    #endregion Changing Values

                    #region Restrict Volume Limit to 0.0f - 1.0f

                    // Sound Effect Volume
                    if( sVolume > 1.0 )
                        sVolume = 1.0f;
                    else if( sVolume < 0 )
                        sVolume = 0;
                    // Music Volume
                    if( mVolume > 1.0 )
                        mVolume = 1.0f;
                    else if( mVolume < 0 )
                        mVolume = 0;

                    #endregion Restrict Volume Limit to 0.0f - 1.0f

                    // Set Volume
                    SoundEffect.MasterVolume = sVolume;
                    MediaPlayer.Volume = mVolume;
                    // Reset the boolean so other Keyboard/Mouse relient command will not work
                    bKstate = false;
                    bMstate = false;
                    mScroll = 0;
                }
            }
            if( Music.songs.Count  > 0 )
                m.MMusic(GraphicsDevice);

            #endregion Music/Sound Volume

            // Run only if Window is actively selected and the mouse is within the Window's boundaries and debug mode allow pausing
            if( IsActive && new Rectangle(0, 0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height).Contains(Mouse.GetState().X, Mouse.GetState().Y) || !aPause ) {
                if( !pause ) { // If game is NOT paused
                    if( menu >= 0.2 && bKstate && menu != 1 && menu != 2 && menu != 3 && menu != 1.2 && menu != 2.2 && menu != 3.2 ) { // Mode
                        if( Keyboard.GetState().IsKeyDown(Keys.H) ) {
                            hidden = !hidden;
                        } else if( Keyboard.GetState().IsKeyDown(Keys.R) ) {
                            rHidden = !rHidden;
                        }
                        if( hidden && rHidden )
                            mode = "IMPOSSIBLE!";
                        else if( hidden )
                            mode = "Hidden!";
                        else if( rHidden )
                            mode = "Revesre Hidden!";
                        else
                            mode = "";
                    }
                    if( menu < 1 ) { // if user is at game menu

                        #region Menu Mech

                        if( menu == 0.0 ) {

                            #region Main Menu

                            // "Fade"
                            mAlpha += 0.1f;
                            iAlpha -= 0.1f;
                            gAlpha -= 0.1f;

                            #region Input Detection

                            // Prevent Option from passing the allowed range
                            if( option < 0 )
                                option = 2;
                            if( option > 2 )
                                option = 0;
                            if( bKstate || Main.mScroll != 0 || bMstate ) {
                                if( Keyboard.GetState().IsKeyDown(Keys.Up) || mScroll > 0 ) {// If user press up
                                    tick.Play();
                                    option--;
                                } else if( Keyboard.GetState().IsKeyDown(Keys.Down) || mScroll < 0 ) { // If user press down
                                    tick.Play();
                                    option++;
                                } else if( Keyboard.GetState().IsKeyDown(Keys.Enter) || Mouse.GetState().LeftButton == ButtonState.Pressed ) {

                                    #region Respond

                                    hit.Play();
                                    if( option == 0 ) { // Game Mode Menu
                                        option = 3;
                                        menu = 0.2;
                                    } else if( option == 1 ) { // Instruction Menu
                                        command = "";
                                        menu = 0.1;
                                    } else if( option == 2 )  // Exit
                                        cExit();

                                    #endregion Respond
                                }
                            }

                            #endregion Input Detection

                            #endregion Main Menu
                        } else if( menu == 0.1 ) {

                            #region Instruction

                            // "Fade"
                            mAlpha -= 0.1f;
                            iAlpha += 0.1f;
                            gAlpha -= 0.1f;

                            if( !Keyboard.GetState().GetPressedKeys().Contains(Keys.LeftShift) && !Keyboard.GetState().GetPressedKeys().Contains(Keys.LeftControl) ) { // Scrolling Instruction screen (Not relient on state boolean)

                                #region Scrolling

                                if( Main.mScroll != 0 )
                                    iMove.Y += mScroll * 400;// Scroll Upward or Downwarad (Based on the Mouse Scroll Wheel)
                                if( Keyboard.GetState().IsKeyDown(Keys.Up) )
                                    iMove.Y += 100; // Scroll Up
                                else if( Keyboard.GetState().IsKeyDown(Keys.Down) )
                                    iMove.Y += -100; // Scroll Down

                                #endregion Scrolling
                            }
                            if( Keyboard.GetState().IsKeyDown(Keys.Enter) && command.Length > 0 ) {

                                #region Commands

                                if( command.ToLower().Equals("fullscreen") ) {
                                    if( g.IsFullScreen ) { // If user is in fullscreen mode
                                        g.IsFullScreen = false; // False
                                        // Reset resolution
                                        g.PreferredBackBufferWidth = 1280;
                                        g.PreferredBackBufferHeight = 950;
                                    } else {
                                        g.IsFullScreen = true; // True
                                        // Reset Resolution
                                        g.PreferredBackBufferWidth = 1280;
                                        g.PreferredBackBufferHeight = 1024;
                                    }

                                    g.ApplyChanges(); // Apply the changes (or else it will not work)
                                } else if( command.ToLower().Equals("skin") ) {
                                    menu = 0.11; // Go to custom skin
                                } else if( command.ToLower().Equals("dance") )
                                    dance = !dance; // Turn off or no Visualizer
                                else if( command.ToLower().Equals("easterseggs") )
                                    easter = !easter;
                                else if( command.ToLower().Equals("reset") ) { // Reset all setting to default
                                    dance = true; // Turn on Visualizer
                                    // Reset resolution base on fullscreen or windowed
                                    if( g.IsFullScreen ) {
                                        g.PreferredBackBufferWidth = 1280;
                                        g.PreferredBackBufferHeight = 1024;
                                    } else if( !g.IsFullScreen ) {
                                        g.PreferredBackBufferWidth = 1280;
                                        g.PreferredBackBufferHeight = 950;
                                    }
                                    g.ApplyChanges(); // Apply the changes
                                } else if( command.ToLower().Equals("picture") )
                                    picture = !picture; // Show music picture (on/off)
                                else if( command.ToLower().Equals("song") ) {
                                    if( !Exiting ) {
                                        AddMusic am = new AddMusic(); // Show the windows form box to add music
                                        am.Show(); // Show box
                                        pause = true; // Pause game
                                    } else if( Exiting ) // If Dodger.exe is still adding songs/pictures
                                        System.Windows.Forms.MessageBox.Show("Dodger.exe is still adding songs/pictures. Please wait until it is finish before adding any more. You can look to the bottom right corner to see any current activities that Dodger.exe is doing.", "Error Start New Thread", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                                } else if( command.ToLower().Equals("debug") )
                                    debug = !debug; // Turn debug on or off
                                if( debug ) {
                                    if( command.ToLower().Equals("collison") )
                                        check = !check; // Allow collison detection or not
                                    if( command.ToLower().Equals("pause") )
                                        aPause = !aPause; // Allow pausing or not
                                }
                                command = ""; // Reset command

                                #endregion Commands
                            } else
                                command = kState.Keystroke(command, bKstate); // Get the user input

                            // Back
                            if( bKstate && Keyboard.GetState().IsKeyDown(Keys.Back) || bMstate && Mouse.GetState().RightButton == ButtonState.Pressed ) {
                                back.Play();
                                menu = 0.0f;
                            }

                            #endregion Instruction
                        } else if( menu == 0.11 ) {

                            #region Custom Skin

                            if( bKstate && Keyboard.GetState().GetPressedKeys().ToList().ContainList(new List<Keys>() { Keys.Up, Keys.Down }) || mScroll != 0 ) {
                                // Reset animation indexes
                                iStext = 0;
                                iWall = 0;
                                iCircle = 0;
                                iError = 0; // Reset error index
                                if( Keyboard.GetState().IsKeyDown(Keys.Up) || mScroll > 0 )
                                    index++; // Increase the skin path index to the next one
                                else if( Keyboard.GetState().IsKeyDown(Keys.Down) || mScroll < 0 )
                                    index--; // Decrease index
                                // Restrict value
                                if( index > sPath.Count - 1 )
                                    index = 0;
                                else if( index < 0 )
                                    index = sPath.Count - 1;
                                SkinPath = sPath[index] + "\\"; // Set new Skin Path
                                LoadSkin(); // Load the new skin

                                tick.Play();
                            }
                            // Back
                            if( bKstate && Keyboard.GetState().IsKeyDown(Keys.Back) || bMstate && Mouse.GetState().RightButton == ButtonState.Pressed ) {
                                back.Play();
                                menu = 0.1;
                            }

                            #endregion Custom Skin
                        } else if( menu == 0.2 ) {

                            #region Game Mode

                            // "Fade"
                            mAlpha -= 0.1f;
                            iAlpha -= 0.1f;
                            gAlpha += 0.1f;

                            #region Input Detection

                            // Prevent Option from passing the allowed range
                            if( option < 3 )
                                option = 5;
                            if( option > 5 )
                                option = 3;
                            if( bKstate || Main.mScroll != 0 || Main.bMstate ) {
                                if( Keyboard.GetState().IsKeyDown(Keys.Left) || mScroll > 0 ) {// If user press left
                                    tick.Play();
                                    option--;
                                } else if( Keyboard.GetState().IsKeyDown(Keys.Right) || mScroll < 0 ) {  // If user press right
                                    tick.Play();
                                    option++;
                                } else if( Keyboard.GetState().IsKeyDown(Keys.Enter) || Mouse.GetState().LeftButton == ButtonState.Pressed ) {
                                    hit.Play();
                                    if( option == 3 ) {
                                        menu = 1.1; // Beta Option Screen
                                    } else if( option == 4 ) {
                                        menu = 2.1; // Flappy Bird Screen
                                    } else if( option == 5 ) {
                                        Music.play = true;
                                        menu = 3.1; // Musical Screen
                                    }
                                } else if( Keyboard.GetState().IsKeyDown(Keys.Back) || Mouse.GetState().RightButton == ButtonState.Pressed ) {
                                    back.Play();
                                    menu = 0.0; // Go back to main menu
                                    option = 0;
                                }
                            }

                            #endregion Input Detection

                            #endregion Game Mode
                        }

                        #region Alpha (Restict Value)

                        if( mAlpha < 0f )
                            mAlpha = 0f;
                        else if( mAlpha > 1.0f )
                            mAlpha = 1.0f;
                        if( gAlpha < 0f )
                            gAlpha = 0f;
                        else if( gAlpha > 1.0f )
                            gAlpha = 1.0f;
                        if( iAlpha < 0f )
                            iAlpha = 0f;
                        else if( iAlpha > 1.0f )
                            iAlpha = 1.0f;

                        #endregion Alpha (Restict Value)
                    } else {
                        p.Update(); // Run the gameplay update method (Let the game START!)
                    }

                        #endregion Menu Mech
                } else if( menu >= 1 && pause ) {

                    #region Pause Mech

                    // Prevent Option from passing the allowed range
                    if( option < 6 )
                        option = 8;
                    if( option > 8 )
                        option = 6;
                    if( bKstate || bMstate || mScroll != 0 ) {
                        if( Keyboard.GetState().IsKeyDown(Keys.Up) || mScroll > 0 ) {// If user press up
                            tick.Play();
                            option--;
                        } else if( Keyboard.GetState().IsKeyDown(Keys.Down) || mScroll < 0 ) { // If user press down
                            tick.Play();
                            option++;
                        } else if( Keyboard.GetState().IsKeyDown(Keys.Left) )
                            bAlpha += 0.1f; // Raise the Dimming
                        else if( Keyboard.GetState().IsKeyDown(Keys.Right) )
                            bAlpha -= 0.1f; // Lower the Dimming
                        bAlpha = MathHelper.Clamp(bAlpha, 0, 1);
                        if( Keyboard.GetState().IsKeyDown(Keys.Enter) || Mouse.GetState().LeftButton == ButtonState.Pressed ) {

                            #region Respond

                            if( menu == 3 )
                                MediaPlayer.Resume();
                            hit.Play();
                            pause = false; // Turn off pause
                            if( option == 6 )
                                p.SDraw.Resume(); // Start the Object thread again
                            else if( option == 7 || option == 8 ) { // Restart/Exit
                                p.SDraw.Resume(); // Start Object thread to...
                                p.SDraw.Abort(); // ...Abort it
                                p.CCheck.Abort(); // Stop the Collison Checking
                                sObject.Clear(); // Clear all Object from the List
                                GamePlay.move = Vector2.Zero; // Reset the Circle move
                                GamePlay.pos = new Vector2(200, 400); // Reset the Circle position
                                GamePlay.prefect = true; // Allow a Prefect gameplay

                                if( option == 7 ) // Restart
                                    menu += 0.1; // Go to the option screen
                                else { // Exit
                                    menu = 0.0;
                                    option = 0;
                                }
                            }

                            #endregion Respond
                        }
                    }

                    #endregion Pause Mech
                }
            } else if( menu == 1 || menu == 2 || menu == 3 ) { // If during gameplay and Window is NOT active or mouse is off screen

                #region Pause Gameplay

                if( menu == 3 )
                    MediaPlayer.Pause(); // Pause the music if play Musical Mode
                pause = true; // Pause the Game
                try {
                    p.SDraw.Suspend(); // Pause the Object Thread
                } catch( ThreadStateException ) { } // If for some reason the Object Thread is not started...

                #endregion Pause Gameplay
            }
            if( bKstate && Keyboard.GetState().IsKeyDown(Keys.Escape) ) { // If user press Escape

                #region Escape and Middle

                back.Play();
                if( menu == 0 )
                    cExit();// Quit the game if at Main Menu
                else if( menu == 1 || menu == 2 || menu == 3 ) { // If during gameplay
                    option = 6;
                    pause = !pause; // Pause or Resume
                    if( menu == 3 && pause )
                        MediaPlayer.Pause(); // Stop Music
                    else if( menu == 3 && !pause )
                        MediaPlayer.Resume(); // Resume Music
                    if( pause )
                        p.SDraw.Suspend(); // Pause Thread
                    else if( !pause )
                        p.SDraw.Resume(); // Resume Thread
                } else { // If at any other screen, go back to Main Menu
                    option = 0;
                    menu = 0;
                }

                #endregion Escape and Middle
            }

            #region Move Arrow

            #region Rotation

            if( menu == 0 || pause ) {
                if( rot < 0 )
                    rot += MathHelper.Pi / 150; // Rotate the arrow Right
                else if( rot > 0 )
                    rot = 0;
            } else if( menu == 0.2 ) {
                if( rot > -MathHelper.PiOver2 )
                    rot -= MathHelper.Pi / 150; // Rotate the arrow up
                else if( rot < -MathHelper.PiOver2 )
                    rot = -MathHelper.PiOver2;
            }

            #endregion Rotation

            if( option == 0 ) { // Start
                move = (pos - new Vector2(725, 660)) * 2;
            } else if( option == 1 ) { // Instruction
                move = (pos - new Vector2(725, 793)) * 2;
            } else if( option == 2 ) { // Quit
                move = (pos - new Vector2(725, 925)) * 2;
            } else if( option == 3 ) { // Beta Mode
                move = (pos - new Vector2(250, 550)) * 2;
            } else if( option == 4 ) { // "Flappy Bird" Mode
                move = (pos - new Vector2(625, 550)) * 2;
            } else if( option == 5 ) { // Musical Mode
                move = (pos - new Vector2(995, 550)) * 2;
            } else if( option == 6 ) { // Resume
                move = (pos - new Vector2(705, 250)) * 2;
            } else if( option == 7 ) { // Restart
                move = (pos - new Vector2(705, 435)) * 2;
            } else if( option == 8 ) { // Quit Mode
                move = (pos - new Vector2(705, 620)) * 2;
            }
            pos -= move * gTime;

            #endregion Move Arrow

            #region Instruction Scrolling

            // Limit Screen Position
            if( instruct.Y > 50 + iMove.Y / 50 )
                iMove.Y -= Math.Abs(iMove.Y / 5); // Slowly Push the Screen Down
            else if( instruct.Y < -1500 + iMove.Y / 50 )
                iMove.Y += Math.Abs((iMove.Y + 1450) / 5); // Slowly Push the Screen Up

            // Change Screen Position
            instruct -= (instruct - iMove) * gTime;

            #endregion Instruction Scrolling

            base.Update(gameTime);
        }

        /// <summary>
        /// Draw
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw( GameTime gameTime ) {

            #region What?

            if( menu < 1 )
                GraphicsDevice.Clear(new Color(GamePlay.sCol.R + 20, GamePlay.sCol.G + 20, GamePlay.sCol.B + 20)); // Colorful Screen
            else if( menu == 1 || menu == 2 || menu == 3 )
                GraphicsDevice.Clear(GamePlay.col); // Beautifull Lights...
            else
                GraphicsDevice.Clear(Color.LightGray); // So Grim and Dull :(

            #endregion What?

            #region Visual

            s.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend); // Draw the image Immediately and Blend all alpha channel

            #region Music Picture

            if( menu == 0.0 || menu == 1 || menu == 2 || menu == 3 || menu == 3.1 ) // Draw the Music Picture
                // Bad Pratice... DON't Do Something Like This
                // No "{ ... }" (Not needed because of try and catch, it counts as 1 statement)
                try {
                    if (Music.songs[Music.index].sPict != null && picture)
                    { // If there is a picture and user allows it
                        if( menu == 3.1 || menu == 0 ) // For during Musical Mode or Main Menu
                            s.Draw(Music.songs[Music.index].sPict, new Rectangle(0, 0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height), new Color(225, 225, 225, 220)); // Draw the picture but Lighten it a bit
                        else
                            s.Draw(Music.songs[Music.index].sPict, new Rectangle(0, 0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height), Color.White); // Draw the picture with all it's glory
                        if( menu != 0 )
                            s.Draw(black, new Rectangle(0, 0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height), new Color(0, 0, 0, bAlpha)); // Draw the Dimming for Musical Mode
                    }
                } catch( ArgumentOutOfRangeException ) { } // Out of Range?? HOW???

            #endregion Music Picture

            #region Dance

            if( dance ) { // If user allow dancing
                // The if's are just placements
                if( menu == 0 )
                    m.Visualize(GraphicsDevice.Viewport, s, wall[iWall], new Vector2(0, GraphicsDevice.Viewport.Height / 2 + 50));
                else if( menu == 0.1 ) { } else if( menu == 3.1 ) {
                    m.Visualize(GraphicsDevice.Viewport, s, wall[iWall], new Vector2(GraphicsDevice.Viewport.Width - 250, 0));
                } else
                    m.Visualize(GraphicsDevice.Viewport, s, wall[iWall], new Vector2(0, GraphicsDevice.Viewport.Height - 100));
            }

            #endregion Dance

            s.End();

            #endregion Visual

            #region Text

            s.Begin(SpriteSortMode.Immediate, BlendState.NonPremultiplied);

            #region Music and Sound Volume

            if( Keyboard.GetState().IsKeyDown(Keys.LeftControl) || Keyboard.GetState().IsKeyDown(Keys.LeftShift) || menu == 0.1 ) {
                if( menu != 1 & menu != 2 && menu != 3 || pause ) { // Draw the sound and music volume
                    s.DrawString(font, "Music: " + (MediaPlayer.Volume * 100).ToString("###"), new Vector2(0, GraphicsDevice.Viewport.Height - 25), Color.Black);
                    s.DrawString(font, "Sound: " + (SoundEffect.MasterVolume * 100).ToString("###"), new Vector2(0, GraphicsDevice.Viewport.Height - 50), Color.Black);
                }
            }

            #endregion Music and Sound Volume

            #region Song Info

            if( MediaPlayer.State == MediaState.Playing && menu != 3.1 && menu != 0.1 && menu != 0.11 ) { // If a song is playing
                string song = ( int )MediaPlayer.PlayPosition.TotalMinutes + "." + (MediaPlayer.PlayPosition.TotalSeconds - ( int )(MediaPlayer.PlayPosition.TotalSeconds / 60) * 60).ToString("00") + " | " + ( int )MediaPlayer.Queue.ActiveSong.Duration.TotalMinutes + "." + (MediaPlayer.Queue.ActiveSong.Duration.TotalSeconds - ( int )(MediaPlayer.Queue.ActiveSong.Duration.TotalSeconds / 60) * 60).ToString("00") + "              "; // Song Duration
                string sName = MediaPlayer.Queue.ActiveSong.ToString().Remove(0, MediaPlayer.Queue.ActiveSong.ToString().LastIndexOf("\\") + 1);
                if( menu == 1 || menu == 2 || menu == 3 ) // Musical Mode Info (Sleeptime and Speed)
                    song += "\nSpawn: " + p.sleepTime + " || Speed: " + GamePlay.mObject;

                s.Draw(black, new Rectangle(( int )(GraphicsDevice.Viewport.Width - fSong.MeasureString(song).X), 22, GraphicsDevice.Viewport.Width, ( int )fSong.MeasureString(song).Y), Color.Black); // Song's Title Black Bar
                s.Draw(black, new Rectangle(( int )(GraphicsDevice.Viewport.Width - fSong.MeasureString(sName).X), 0, GraphicsDevice.Viewport.Width, ( int )fSong.MeasureString(sName).Y), Color.Black); // Duration Black Bar
                s.Draw(black, new Rectangle(GraphicsDevice.Viewport.Width - 200, 30, ( int )(200 * MediaPlayer.PlayPosition.TotalMinutes / MediaPlayer.Queue.ActiveSong.Duration.TotalMinutes), 5), Color.WhiteSmoke); // Duration Bar

                s.DrawString(fSong, sName, new Vector2(GraphicsDevice.Viewport.Width - fSong.MeasureString(sName).X, 0), Color.WhiteSmoke); // Draw the strings (Name)
                s.DrawString(fSong, song, new Vector2(GraphicsDevice.Viewport.Width - fSong.MeasureString(song).X, 22), Color.White); // Duration
            }

            #endregion Song Info

            #region Convertion

            s.DrawString(font, sConvert, new Vector2(GraphicsDevice.Viewport.Width - font.MeasureString(sConvert).X, GraphicsDevice.Viewport.Height - 25), Color.Black);

            #endregion Convertion

            #region Debug

            if( debug ) {
                string mousepos = "X: " + ( int )mState.X + " Y: " + ( int )mState.Y + "\nMenu: " + menu + " Game Time: " + gTime; // Get Numbers
                if( menu == 1.15 ) // Get Beta Info
                    mousepos += "\nSpawn: " + p.sleepTime + " Type of Object: " + p.sObject + "\nMultiple: " + p.multi + " Percent: " + p.percent;
                if( menu == 1 || menu == 2 || menu == 3 ) { // Get Game Info
                    mousepos += "\nPosition: " + GamePlay.pos.ToString() + " || Spawn Time: " + p.sleepTime + "\nmObject: " + GamePlay.mObject + " || Mutli: " + p.multi + " || Speedy: " + p.speedy + " || Jump Height: " + p.bHeight + " Gravity: " + p.bGravity;
                }
                s.Draw(black, new Rectangle(0, ( int )(GraphicsDevice.Viewport.Height - font.MeasureString(mousepos).Y), ( int )font.MeasureString(mousepos).X, ( int )font.MeasureString(mousepos).Y), Color.Black);
                s.DrawString(font, mousepos, new Vector2(0, GraphicsDevice.Viewport.Height - font.MeasureString(mousepos).Y), Color.White); // Draw Text
                s.Draw(black, new Rectangle(( int )mState.Position().X - 5, ( int )mState.Position().Y - 5, 5, 5), null, Color.Red, MathHelper.PiOver4, new Vector2(.5f, .5f), SpriteEffects.None, 0);
            }

            #endregion Debug

            s.End();

            #endregion Text

            #region Game

            s.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend); // Draw all Object Immediately, Blend all Alpha
            if( mode != "" && menu >= 0.2 && menu != 1 && menu != 2 && menu != 3 && menu != 1.2 && menu != 2.2 && menu != 3.2 )
                s.DrawString(font, mode, new Vector2(300, 0), Color.Black);

            if( menu < 1 ) {

                #region Main Menu

                if( menu == 0 )
                    s.Draw(sMenu, Vector2.Zero, new Color(255, 255, 255, mAlpha)); // Main Menu
                else if( menu == 0.1 ) {

                    #region Instruction

                    s.Draw(sInstruct, instruct, new Color(255, 255, 255, iAlpha)); // Draw the Instruction
                    s.DrawString(font, command, new Vector2(GraphicsDevice.Viewport.Width - font.MeasureString(command).X, 0), Color.Black); // Draw the Commands
                    // Option Statements
                    s.Draw(wall[iWall], new Vector2(550, 650 + instruct.Y), Color.White); // Wall Sprite
                    s.Draw(circle[iCircle], new Vector2(350, 850 + instruct.Y), Color.White); // Object Sprite
                    // Boolean Statements
                    s.DrawString(fMenu, "(" + g.IsFullScreen.ToString() + ")", new Vector2(972, 2005 + instruct.Y), Color.Orange);
                    s.DrawString(fMenu, "(" + picture.ToString() + ")", new Vector2(813, 2061 + instruct.Y), Color.Orange);
                    s.DrawString(fMenu, "(" + dance.ToString() + ")", new Vector2(920, 2115 + instruct.Y), Color.Orange);

                    #endregion Instruction
                } else if( menu == .11 ) {

                    #region Custom Skin

                    s.DrawString(fMenu, "Chose a Skin!", Vector2.Zero, Color.Black); // Title
                    s.DrawString(font, sPath[index], new Vector2(10, 50), Color.Black); // Path Name
                    s.Draw(sText[iStext], new Vector2(200, 400), Color.White); // Player Piece
                    s.Draw(wall[iWall], new Vector2(300, 60), Color.White); // Wall Sprite
                    s.Draw(circle[iCircle], new Vector2(750, 200), Color.White); // Circle Sprite

                    #endregion Custom Skin

                    #region Errors

                    if( errors.Count - 1 >= 0 ) {
                        s.DrawString(font, errors[iError], new Vector2(300, 10), Color.Black); // Errors
                        iError++;
                        if( iError > errors.Count - 1 )
                            iError = 0;
                    }

                    #endregion Errors
                } else if( menu == .2 )
                    s.Draw(sMode, Vector2.Zero, new Color(255, 255, 255, gAlpha)); // Game Mode
                // Draw the arrow with rotation
                if( menu == 0 || menu == 0.2 )
                    s.Draw(aText, pos, null, Color.White, rot, new Vector2(aText.Width / 2, aText.Height / 2), 1.0f, SpriteEffects.None, 0f);

                #endregion Main Menu
            } else
                p.Draw(s, font);  // Draw the GamePlay
            if( pause && menu >= 1 ) { // Draw the Pause Screen
                s.Draw(sPause, Vector2.Zero, Color.White);
                s.Draw(aText, pos, null, Color.White, rot, new Vector2(aText.Width / 2, aText.Height / 2), 1.0f, SpriteEffects.None, 0f); // Draw the arrow with rotation
            }
            s.End();

            #endregion Game

            #region Texture List

            // Increase Animation
            iStext++;
            iWall++;
            iCircle++;
            // Restict Range
            if( iStext > sText.Count - 1 )
                iStext = 0;
            if( iWall > wall.Count - 1 )
                iWall = 0;
            if( iCircle > circle.Count - 1 )
                iCircle = 0;

            #endregion Texture List

            base.Draw(gameTime);
        }

        #endregion Update/Draw

        /// <summary>
        /// Check any for any left over files and exit
        /// </summary>
        private void cExit() {
            Exiting = true; // Tell Convert to finish up and end
            if( Directory.Exists(cDir + "\\Music\\Unused Files") && Directory.GetFiles(cDir + "\\Music\\Unused Files").Count() - 1 >= 0 ) {
                Unused uFile = new Unused();
                uFile.Show();
                pause = true;
            } else
                this.Exit();
        }

        /// <summary>
        /// Write Config File
        /// Finish Up Any Convertion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected override void OnExiting( object sender, EventArgs args ) {
            base.OnExiting(sender, args);
            MediaPlayer.Stop(); // Stop Music

            #region Update Config

            using( StreamWriter sr = File.CreateText(".\\Config.db") ) { // Update Config
                sr.WriteLine("Skin Path:" + SkinPath); // Current Skin
                sr.WriteLine("Sound:" + SoundEffect.MasterVolume); // Sound Volume
                sr.WriteLine("Music:" + MediaPlayer.Volume); // Music Volume
                sr.WriteLine("Dance:" + dance); // Dance?
                sr.WriteLine("Picture:" + picture); // Picture?
                sr.WriteLine("Fullscreen:" + g.IsFullScreen); // Fullscreen?
            }

            #endregion Update Config

            #region Stop Being Lame

            if( cSong.IsAlive ) { // Clear out the unfinish process
                if( sConvert.Contains("-->") ) {
                    cSong.Abort(); // Abort the thread
                    pLame.Kill(); // Stop the lame.exe
                    if( sConvert.Contains(" --> Decoding") )
                        File.Delete(cDir + "\\Music\\Temp"); // Delete the temp file
                    else if( sConvert.Contains(" --> Recoding") ) {
                        File.Delete(cDir + "\\Music\\Temp"); // Delete the temp file
                        File.Delete(cDir + "\\Music\\Music\\" + sConvert.Remove(sConvert.IndexOf("-") - 1) + ".mp3"); // Delete Unfinished File
                    }
                }
                while( cSong.IsAlive ) { } // Wait for it to add the song to the song list
            }

            #endregion Stop Being Lame

            #region Song Thingy

            List<string> temp = new List<string>(); // Temp list
            foreach (Music.SongsList sl in Music.songs)
            {
                temp.Add(".\\Music\\Music\\" + sl.sSong.Name + ".mp3"); // Make a list of all song names
            }
            File.WriteAllLines(".\\Songs.db", temp); // Create the list

            #endregion Song Thingy
        }
    }
}