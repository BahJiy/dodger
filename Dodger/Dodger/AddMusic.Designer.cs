﻿namespace Dodger
{
    partial class AddMusic
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MusicLocation = new System.Windows.Forms.TextBox();
            this.PictureLocation = new System.Windows.Forms.TextBox();
            this.MusicLabel = new System.Windows.Forms.Label();
            this.PictureLabel = new System.Windows.Forms.Label();
            this.BrowseMusic = new System.Windows.Forms.Button();
            this.BrowsePicture = new System.Windows.Forms.Button();
            this.Add = new System.Windows.Forms.Button();
            this.Cancel = new System.Windows.Forms.Button();
            this.SongName = new System.Windows.Forms.TextBox();
            this.SongLabel = new System.Windows.Forms.Label();
            this.Switch = new System.Windows.Forms.Button();
            this.SongBox = new System.Windows.Forms.ComboBox();
            this.SwitchList = new System.Windows.Forms.Button();
            this.Batch = new System.Windows.Forms.Button();
            this.BatchList = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // MusicLocation
            // 
            this.MusicLocation.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.MusicLocation.Cursor = System.Windows.Forms.Cursors.No;
            this.MusicLocation.Location = new System.Drawing.Point(12, 49);
            this.MusicLocation.Name = "MusicLocation";
            this.MusicLocation.ReadOnly = true;
            this.MusicLocation.Size = new System.Drawing.Size(264, 13);
            this.MusicLocation.TabIndex = 0;
            // 
            // PictureLocation
            // 
            this.PictureLocation.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.PictureLocation.Cursor = System.Windows.Forms.Cursors.No;
            this.PictureLocation.Location = new System.Drawing.Point(12, 117);
            this.PictureLocation.Name = "PictureLocation";
            this.PictureLocation.ReadOnly = true;
            this.PictureLocation.Size = new System.Drawing.Size(264, 13);
            this.PictureLocation.TabIndex = 1;
            this.PictureLocation.WordWrap = false;
            // 
            // MusicLabel
            // 
            this.MusicLabel.AutoSize = true;
            this.MusicLabel.Location = new System.Drawing.Point(144, 30);
            this.MusicLabel.Name = "MusicLabel";
            this.MusicLabel.Size = new System.Drawing.Size(79, 13);
            this.MusicLabel.TabIndex = 2;
            this.MusicLabel.Text = "Music Location";
            // 
            // PictureLabel
            // 
            this.PictureLabel.AutoSize = true;
            this.PictureLabel.Location = new System.Drawing.Point(9, 101);
            this.PictureLabel.Name = "PictureLabel";
            this.PictureLabel.Size = new System.Drawing.Size(122, 13);
            this.PictureLabel.TabIndex = 3;
            this.PictureLabel.Text = "Music\'s Picture Location";
            // 
            // BrowseMusic
            // 
            this.BrowseMusic.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BrowseMusic.Location = new System.Drawing.Point(283, 46);
            this.BrowseMusic.Name = "BrowseMusic";
            this.BrowseMusic.Size = new System.Drawing.Size(91, 23);
            this.BrowseMusic.TabIndex = 4;
            this.BrowseMusic.Text = "Browse Music";
            this.BrowseMusic.UseVisualStyleBackColor = true;
            this.BrowseMusic.Click += new System.EventHandler(this.BrowseMusic_Click);
            // 
            // BrowsePicture
            // 
            this.BrowsePicture.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BrowsePicture.Location = new System.Drawing.Point(283, 114);
            this.BrowsePicture.Name = "BrowsePicture";
            this.BrowsePicture.Size = new System.Drawing.Size(91, 23);
            this.BrowsePicture.TabIndex = 5;
            this.BrowsePicture.Text = "Browse Picture";
            this.BrowsePicture.UseVisualStyleBackColor = true;
            this.BrowsePicture.Click += new System.EventHandler(this.BrowsePicture_Click);
            // 
            // Add
            // 
            this.Add.BackColor = System.Drawing.Color.Lime;
            this.Add.Cursor = System.Windows.Forms.Cursors.Cross;
            this.Add.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Add.Location = new System.Drawing.Point(56, 143);
            this.Add.Name = "Add";
            this.Add.Size = new System.Drawing.Size(75, 23);
            this.Add.TabIndex = 6;
            this.Add.Text = "Add Music";
            this.Add.UseVisualStyleBackColor = false;
            this.Add.Click += new System.EventHandler(this.Add_Click);
            // 
            // Cancel
            // 
            this.Cancel.BackColor = System.Drawing.Color.Red;
            this.Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Cancel.Location = new System.Drawing.Point(218, 143);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(75, 23);
            this.Cancel.TabIndex = 7;
            this.Cancel.Text = "Cancel";
            this.Cancel.UseVisualStyleBackColor = false;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // SongName
            // 
            this.SongName.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.SongName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SongName.Location = new System.Drawing.Point(134, 75);
            this.SongName.Name = "SongName";
            this.SongName.Size = new System.Drawing.Size(240, 13);
            this.SongName.TabIndex = 8;
            // 
            // SongLabel
            // 
            this.SongLabel.AutoSize = true;
            this.SongLabel.Location = new System.Drawing.Point(9, 78);
            this.SongLabel.Name = "SongLabel";
            this.SongLabel.Size = new System.Drawing.Size(119, 13);
            this.SongLabel.TabIndex = 9;
            this.SongLabel.Text = "Type in the Song Name";
            // 
            // Switch
            // 
            this.Switch.Location = new System.Drawing.Point(2, 2);
            this.Switch.Name = "Switch";
            this.Switch.Size = new System.Drawing.Size(103, 23);
            this.Switch.TabIndex = 10;
            this.Switch.Text = "Song and Pictures";
            this.Switch.UseVisualStyleBackColor = true;
            this.Switch.Click += new System.EventHandler(this.Switch_Click);
            // 
            // SongBox
            // 
            this.SongBox.FormattingEnabled = true;
            this.SongBox.Location = new System.Drawing.Point(11, 101);
            this.SongBox.Name = "SongBox";
            this.SongBox.Size = new System.Drawing.Size(265, 21);
            this.SongBox.TabIndex = 12;
            this.SongBox.Visible = false;
            // 
            // SwitchList
            // 
            this.SwitchList.Location = new System.Drawing.Point(12, 46);
            this.SwitchList.Name = "SwitchList";
            this.SwitchList.Size = new System.Drawing.Size(230, 23);
            this.SwitchList.TabIndex = 13;
            this.SwitchList.Text = "All Songs";
            this.SwitchList.UseVisualStyleBackColor = true;
            this.SwitchList.Visible = false;
            this.SwitchList.Click += new System.EventHandler(this.SwitchList_Click);
            // 
            // Batch
            // 
            this.Batch.BackColor = System.Drawing.Color.Lime;
            this.Batch.Cursor = System.Windows.Forms.Cursors.Cross;
            this.Batch.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Batch.Location = new System.Drawing.Point(137, 143);
            this.Batch.Name = "Batch";
            this.Batch.Size = new System.Drawing.Size(75, 23);
            this.Batch.TabIndex = 14;
            this.Batch.Text = "Add Batch";
            this.Batch.UseVisualStyleBackColor = false;
            this.Batch.Click += new System.EventHandler(this.Batch_Click);
            // 
            // BatchList
            // 
            this.BatchList.FormattingEnabled = true;
            this.BatchList.Location = new System.Drawing.Point(111, 4);
            this.BatchList.Name = "BatchList";
            this.BatchList.Size = new System.Drawing.Size(263, 21);
            this.BatchList.TabIndex = 15;
            // 
            // AddMusic
            // 
            this.AcceptButton = this.Add;
            this.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.CancelButton = this.Cancel;
            this.ClientSize = new System.Drawing.Size(382, 176);
            this.Controls.Add(this.BatchList);
            this.Controls.Add(this.Batch);
            this.Controls.Add(this.SwitchList);
            this.Controls.Add(this.SongBox);
            this.Controls.Add(this.Switch);
            this.Controls.Add(this.SongLabel);
            this.Controls.Add(this.SongName);
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.Add);
            this.Controls.Add(this.BrowsePicture);
            this.Controls.Add(this.BrowseMusic);
            this.Controls.Add(this.PictureLabel);
            this.Controls.Add(this.MusicLabel);
            this.Controls.Add(this.PictureLocation);
            this.Controls.Add(this.MusicLocation);
            this.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MinimizeBox = false;
            this.Name = "AddMusic";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "AddMusic";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox MusicLocation;
        private System.Windows.Forms.TextBox PictureLocation;
        private System.Windows.Forms.Label MusicLabel;
        private System.Windows.Forms.Label PictureLabel;
        private System.Windows.Forms.Button BrowseMusic;
        private System.Windows.Forms.Button BrowsePicture;
        private System.Windows.Forms.Button Add;
        private System.Windows.Forms.Button Cancel;
        private System.Windows.Forms.TextBox SongName;
        private System.Windows.Forms.Label SongLabel;
        private System.Windows.Forms.Button Switch;
        private System.Windows.Forms.ComboBox SongBox;
        private System.Windows.Forms.Button SwitchList;
        private System.Windows.Forms.Button Batch;
        private System.Windows.Forms.ComboBox BatchList;
    }
}