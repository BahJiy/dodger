﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;

// Custom Library
using Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Dodger {

    /// <summary>
    /// This Class Controls the Game Play.
    /// Call on this to Start Game
    /// </summary>
    internal class GamePlay {

        #region Game Play

        #region Variables

        private string dScore = "";

        private bool
            death = true,
            singleSong = false; // Single Song Musical Mode

        public static bool prefect = true; // For Prefect Game Play

        private int
            // Beta
            bSpawn = 6, // Spawn Time
            bSpeed = 6, // Object Speed
            bObject = 1, // Type of Object
            bMulti = 1, // Multiple At Once?
            bSpeedy = 1, // * Speed
            nSpawn = 0, // Normal
            musicIndex = 0; // For Single Song

        private double
            bHscore = 0, // Beta High Score
            nHscore = 0, // Normal High Score
            mHscore = 0; // Musical High Score

        public int
            bHeight = 75, // Jump Height
            bGravity = 5; // Gravity

        private Random rand = new Random();
        private List<SongScore> sSong = new List<SongScore>();
        public Thread SDraw, CCheck;

        #endregion Variables

        public class SongScore {

            public string Name { get; set; }

            public double Score { get; set; }
        }

        public GamePlay() {
            for( int x = Music.songs.Count - 1; x >= 0; x-- ) {
                sSong.Add(new SongScore { Name = Music.songs[x].sSong.Name, Score = 0 });
            }
        }

        /// <summary>
        /// Update the Game Play
        /// </summary>
        public void Update() {
            if( Main.menu > 1 && Main.menu <= 1.17 || Main.menu >= 2.1 && Main.menu <= 2.11 || Main.menu >= 3.1 || Main.menu == 1.2 || Main.menu == 2.2 || Main.menu == 3.2 ) { // Run ONLY if the user is at an option screen
                if( Main.menu > 1 && Main.menu < 1.17 || Main.menu == 2.1 || Main.menu == 3.1 ) { // If user is changing options
                    if( Main.menu == 3.1 ) { // For Musical because it sooo special
                        if( Main.bKstate && Keyboard.GetState().IsKeyDown(Keys.LeftShift) )
                            singleSong = !singleSong; // Activate/Deactivate Single Song Play
                        if( Main.bKstate && Keyboard.GetState().IsKeyDown(Keys.Delete) ) { // To Delete Songs
                            File.Delete(Directory.GetCurrentDirectory() + "\\Music\\Music\\" + Music.songs[Music.index].sSong.Name + ".mp3");
                            Music.index++;
                            Music.songs.RemoveAt(Music.index - 1);
                            MediaPlayer.Stop();
                        }
                    }

                    #region Increase/Decrease

                    if( Main.mScroll > 0 || Main.bKstate && Keyboard.GetState().IsKeyDown(Keys.Up) ) {
                        Main.tick.Play();

                        #region Beta Increase

                        if( Main.menu == 1.10 ) // Spawner Setting
                            bSpawn++;
                        else if( Main.menu == 1.11 ) // Speed Setting
                            bSpeed++;
                        else if( Main.menu == 1.12 ) // Object Setting
                            bObject++;
                        else if( Main.menu == 1.13 ) // Multi Setting
                            bMulti++;
                        else if( Main.menu == 1.14 ) // Speedy Setting
                            bSpeedy += 2;
                        else if( Main.menu == 1.15 ) // Jump Setting
                            bHeight += 5;
                        else if( Main.menu == 1.16 ) // Gravity Setting
                            bGravity += 2;

                        #endregion Beta Increase

                        #region Normal Increase

                        if( Main.menu == 2.1 ) {
                            nSpawn++; // Increase Value
                            if( nSpawn > 1 )
                                nSpawn = 0; // Change Value
                        }

                        #endregion Normal Increase

                        #region Music Increase

                        if( Main.menu == 3.1 ) {
                            Music.index++;
                            MediaPlayer.Stop();
                        }

                        #endregion Music Increase
                    } else if( Main.mScroll < 0 || Main.bKstate && Keyboard.GetState().IsKeyDown(Keys.Down) ) {
                        Main.tick.Play();

                        #region Beta Decrease

                        if( Main.menu == 1.10 ) // Spawner Setting
                            bSpawn--;
                        else if( Main.menu == 1.11 ) // Speed Setting
                            bSpeed--;
                        else if( Main.menu == 1.12 ) // Object Setting
                            bObject--;
                        else if( Main.menu == 1.13 ) // Multi Setting
                            bMulti--;
                        else if( Main.menu == 1.14 ) // Speedy Setting
                            bSpeedy -= 2;
                        else if( Main.menu == 1.15 ) // Jump Setting
                            bHeight -= 5;
                        else if( Main.menu == 1.16 ) // Gravity Setting
                            bGravity -= 2;

                        #endregion Beta Decrease

                        #region Normal Decrease

                        if( Main.menu == 2.1 ) {
                            nSpawn--;
                            if( Keyboard.GetState().IsKeyDown(Keys.RightAlt) && nSpawn < 0 )
                                nSpawn = 3; // Easter Egg
                            else if( nSpawn < 0 )
                                nSpawn = 1; // Change Value
                        }

                        #endregion Normal Decrease

                        #region Music Decrease

                        if( Main.menu == 3.1 ) {
                            Music.index--;
                            MediaPlayer.Stop();
                        }

                        #endregion Music Decrease
                    }

                    #endregion Increase/Decrease

                    #region Reset Value

                    if( bSpawn > 25 ) // Spawn Timer
                        bSpawn = 1;
                    else if( bSpawn < 1 )
                        bSpawn = 25;
                    if( bSpeed > 25 ) // Speed
                        bSpeed = 1;
                    else if( bSpeed < 1 )
                        bSpeed = 25;
                    if( bObject > 3 ) // Object Type
                        bObject = 1;
                    else if( bObject < 1 )
                        bObject = 3;
                    if( bMulti > 4 ) // Multi
                        bMulti = 1;
                    else if( bMulti < 1 )
                        bMulti = 4;
                    if( bSpeedy > 100 ) // Speedy
                        bSpeedy = 1;
                    else if( bSpeedy < 1 )
                        bSpeedy = 100;
                    if( bHeight > 100 ) // Jump
                        bHeight = 5;
                    else if( bHeight < 5 )
                        bHeight = 100;
                    if( bGravity > 100 ) // Gravity
                        bGravity = 2;
                    else if( bGravity < 2 )
                        bGravity = 100;

                    #endregion Reset Value
                }

                #region Start Game

                #region Start

                if( Main.menu == 1.17 || Main.menu == 2.11 ) {
                    if( Main.bKstate || Main.bMstate ) {
                        if( !Keyboard.GetState().GetPressedKeys().ToList().ContainList<Keys>(new List<Keys> { Keys.Z, Keys.X, Keys.C, Keys.V, Keys.LeftControl, Keys.LeftShift, Keys.Escape, Keys.Back, Keys.RightControl }) || Main.bMstate ) {// If the Keys Press were not Special Keys
                            if( Main.menu == 1.17 )
                                Main.menu = 1;
                            else if( Main.menu == 2.11 )
                                Main.menu = 2;
                        }
                    }
                }

                #endregion Start

                #region Forward/Backward

                if( Main.bMstate || Main.bKstate ) {
                    if( Keyboard.GetState().IsKeyDown(Keys.Enter) || Mouse.GetState().LeftButton == ButtonState.Pressed ) {

                        #region Forward

                        Main.hit.Play();
                        // Reset Score
                        Main.score = 0;
                        mScore = 0;
                        if( Main.menu > 1 && Main.menu < 1.17 ) {
                            if( Keyboard.GetState().GetPressedKeys().Contains(Keys.RightControl) )
                                Main.menu = 1.17; // Skip To Game
                            else
                                Main.menu += 0.01; // Beta
                            if( Main.menu == 1.17 ) {
                                sleepTime = bSpawn * 100;
                                sObject = bObject;
                                multi = bMulti;
                                percent = bSpeedy;
                                mObject = bSpeed * 10;
                            }
                        } else if( Main.menu == 2.1 ) {
                            sleepTime = 1000;
                            if( nSpawn == 0 ) { // Wall only
                                mObject = 60;
                                sObject = 2;
                            } else { // Circle only
                                mObject = 90;
                                sObject = 3;
                            }
                            Main.menu = 2.11; // Normal
                        } else if( Main.menu == 3.1 ) {// Musical
                            musicIndex = Music.index;
                            Music.index--;
                            MediaPlayer.Stop();
                            Main.menu = 3;
                        } else if( Main.menu == 1.2 || Main.menu == 2.2 || Main.menu == 3.2 )
                            Main.menu -= 0.1; // Death

                        #endregion Forward
                    } else if( Keyboard.GetState().IsKeyDown(Keys.Back) || Mouse.GetState().RightButton == ButtonState.Pressed ) {

                        #region Backward

                        Main.back.Play();
                        if( Main.menu > 1.1 && Main.menu <= 1.17 )
                            Main.menu -= 0.01; // Beta
                        else if( Main.menu == 2.11 )
                            Main.menu = 2.1; // Normal
                        else { // Death
                            // Reset Arrow
                            if( Main.menu > 3 )
                                Main.option = 5;
                            else if( Main.menu > 2 )
                                Main.option = 4;
                            else if( Main.menu > 1 )
                                Main.option = 3;
                            Main.menu = 0.2; // Back to Game Mode
                        }

                        #endregion Backward
                    }
                }

                #endregion Forward/Backward

                #endregion Start Game
            } else if( Main.menu == 1 || Main.menu == 2 || Main.menu == 3 ) {

                #region Game Jump!

                Jump();

                #region Score

                if( Main.score < 0 )
                    Main.score = 0;
                if( Main.sUpdate != Convert.ToInt32(Main.score) )
                    Main.sUpdate += (Main.score - Main.sUpdate) / 10;

                #endregion Score

                #region Threading

                try { // Sprite Drawing
                    if( !SDraw.IsAlive ) {// Check if Object Thread is Alive
                        SDraw = new Thread(sAdd);
                        SDraw.Start(); // If not then start it
                    }
                } catch( NullReferenceException ) {
                    SDraw = new Thread(sAdd);
                }
                try { // Collision
                    if( !CCheck.IsAlive ) {
                        CCheck = new Thread(Check);
                        CCheck.Start();
                    }
                } catch( NullReferenceException ) {
                    CCheck = new Thread(Check);
                }

                #endregion Threading

                #region Music Cal

                if( Main.menu == 3 ) {
                    if( singleSong && musicIndex != Music.index ) {
                        Main.menu = 3.2;
                        Music.index = musicIndex - 1;
                        MediaPlayer.Stop();
                    }
                    List<float> freq = Music.visual.Frequencies.ToList();
                    // Get value from several freq and calculate the speed
                    // The value are spread to make sure that the object move accordingly
                    mObject = ( int )(freq.ValueSum() / .8);
                    bGravity = ( int )(freq.ValueSum(80, 200));
                    bHeight = ( int )(freq.ValueSum(0, 150));
                }

                #endregion Music Cal

                #endregion Game Jump!
            }
        }

        private void ChangeScreen() {
        }

        private void OptionScreen() {
        }

        /// <summary>
        /// Draw the Game Play
        /// </summary>
        public void Draw( SpriteBatch s, SpriteFont font ) {
            try {
                string gText = ""; // Game Text
                if( Main.menu > 1 && Main.menu < 1.2 ) {

                    #region Beta Option Screen

                    if( Main.menu == 1.1 ) // Spawn Timer
                        gText = "Range: 1 - 25\nThis determines the Spawner Timer of the Game.\nThe LOWER the number - the FASTER the objects will spawn; vice versa.";
                    else if( Main.menu == 1.11 ) // Object Speed
                        gText = "Range: 1 - 25\nThis determines the Speed of the Object that spawns.\nThe LOWER the number - the SLOWER the objects will move; vice versa.";
                    else if( Main.menu == 1.12 ) // Type of Object
                        gText = "Range: 1 - 3\nThis determines the Types of Object that spawns.\n1: Both \"Walls\" and \"Circle\" Sprites || 2: \"Walls\" Sprite ONLY || 3: \"Circle\" Sprite ONLY";
                    else if( Main.menu == 1.13 ) // Multi Spawn
                        gText = "Range: 1 - 3\nThis determine how many Object will spawn at once.";
                    else if( Main.menu == 1.14 ) // Speedy
                        gText = "Range: 1 - 100\nThis is the chance that an Object will be faster than normal.\nThe LOWER the number - the LOWER the percentage.";
                    else if( Main.menu == 1.15 ) // Jump
                        gText = "Range: 1 - 100\nThis the \"Jump\" the the Circle will have. Value is time by 10.\nThe LOWER the number - the lower the Ciricle will \"Jump.\"";
                    else if( Main.menu == 1.16 ) // Gravity
                        gText = "Range: 1 - 100\nThis is the Gravity acting towards the Ciricle.\nThe LOWER the number - the slower the Cirlce will fall.";
                    else if( Main.menu == 1.17 )
                        gText = "Ready?\nStart Jumping!";
                    s.DrawString(font, "Regular Options (Press Right Ctrl + Enter to Start Game Early):\n\nObject Spawn Time: " + bSpawn + "      ||      Object Speed: " + bSpeed + "     ||    What Object to Spawn: " + bObject +
                        "\n\nSpecial Option:\n\nSpawn Mutliple: " + bMulti + "      ||   % of Object Speedy: " + bSpeedy + "\n\nJump Setting\n\nJump Height: " + bHeight + "    || Gravity: " + bGravity + "\n" + gText, new Vector2(100, 100), Color.Black);

                    #endregion Beta Option Screen
                } else if( Main.menu == 2.1 || Main.menu == 2.11 ) {

                    #region Normal Option Screen

                    if( nSpawn < 3 ) {
                        if( nSpawn == 0 )
                            gText = "\"Flappy Bird Mode\"";
                        else if( nSpawn == 1 )
                            gText = "Circle Mode";
                        else if( nSpawn == 2 )
                            gText = ":D";

                        s.DrawString(font, "Chose your Mode (All Modes have preset settings):\n" + gText, new Vector2(100, 100), Color.Black);
                        if( Main.menu == 2.15 )
                            s.DrawString(font, "Ready?\nStart Jumping!", new Vector2(100, 150), Color.Black);
                    }

                    #endregion Normal Option Screen
                } else if( Main.menu == 3.1 ) {

                    #region Music Screen

                    try {
                        s.DrawString(font, "Single Song Play: " + singleSong.ToString(), Vector2.Zero, Color.Black);
                        for( int i = 0; i < 7; i++ ) {
                            int iSong = -3 + i + Music.index;
                            if( iSong < 0 )
                                iSong += Music.songs.Count;
                            else if( iSong > Music.songs.Count - 1 )
                                iSong -= Music.songs.Count;

                            string cSong = Music.songs[iSong].sSong.Name;
                            string sName = cSong.Remove(0, cSong.LastIndexOf("\\") + 1);

                            int y = 72 + i * 135;
                            int y2 = 25 + i * 135;
                            if( i == 3 ) {
                                s.Draw(Main.sLay, new Vector2(75, y2), new Color(col.R, col.G, col.B));
                                s.DrawString(font, sName, new Vector2(205, y), Color.Black);
                            } else {
                                s.Draw(Main.sLay, new Vector2(0, y2), new Color(col.R, col.G, col.B));
                                s.DrawString(font, sName, new Vector2(130, y), Color.Black);
                            }
                        }
                    } catch( NullReferenceException ) { } catch( ArgumentNullException ) { }

                    #endregion Music Screen
                } else if( Main.menu == 1 || Main.menu == 2 || Main.menu == 3 ) {
                    // Draw Score
                    s.DrawString(font, "Score: " + Main.sUpdate.ToString("##0.0"), new Vector2(62, 20), sCol);
                    death = true;

                    #region Drawing

                    // Draw Player afterimages
                    for( int x = OLDpos.Count - 1; x >= 0; x-- ) {
                        s.Draw(Main.sText[Main.iStext], OLDpos[x], Color.White);
                    }
                    s.Draw(Main.sText[Main.iStext], pos, Color.White);
                    // Draw the Object. Depend on the number of Objects in the List
                    for( int x = Main.sObject.Count - 1; x >= 0; x-- ) { // Update the Object image
                        Main.sObject[x].Draw(s);
                    }

                    #endregion Drawing
                } else if( Main.menu == 1.2 || Main.menu == 2.2 || Main.menu == 3.2 ) {

                    #region Death

                    if( death ) {
                        if( prefect )
                            dScore = "Prefect Game Play! Great Job not hitting the boundaries or objects!\n";
                        else
                            prefect = true;

                        #region High Score

                        if( Main.menu == 1.2 ) {
                            if( Main.score > bHscore ) {
                                dScore = "You beat your old high score by " + (Main.score - bHscore).ToString("##0.0") + "!";
                                bHscore = Main.score;
                            } else
                                dScore = "Beta High Score: " + bHscore.ToString("##0.0");
                        } else if( Main.menu == 2.2 ) {
                            if( Main.score > nHscore ) {
                                dScore = "You beat your old high score by " + (Main.score - nHscore).ToString("##0.0") + "!";
                                nHscore = Main.score;
                            } else
                                dScore = "Normal High Score: " + nHscore.ToString("##0.0");
                        } else if( Main.menu == 3.2 ) {
                            if( singleSong ) {

                                #region Single Song Score

                                double temp = 0;
                                int tempIndex = 0;
                                for( int x = sSong.Count - 1; x >= 0; x-- ) {
                                    if( sSong[x].Name == MediaPlayer.Queue.ActiveSong.Name ) {
                                        temp = sSong[x].Score;
                                        tempIndex = x;
                                    }
                                }
                                if( mScore > temp ) {
                                    dScore = "You beat your old high score on \"" + MediaPlayer.Queue.ActiveSong.Name + "\" by " + (mScore - temp).ToString("##0.0") + "!";
                                    sSong[tempIndex].Score = mScore;
                                } else
                                    dScore = "\"" + MediaPlayer.Queue.ActiveSong.Name + "\" High Score: " + temp.ToString("##0.0");

                                #endregion Single Song Score
                            } else {

                                #region Mutli Song

                                if( mScore > mHscore ) {
                                    dScore = "You beat your old high score by " + (mScore - mHscore).ToString("##0.0") + "!";
                                    mHscore = mScore;
                                } else
                                    dScore = "Musical High Score: " + mHscore.ToString("##0.0");

                                #endregion Mutli Song
                            }
                        }

                        #endregion High Score

                        #region Clear Data

                        Main.sObject.Clear();
                        pos = new Vector2(200, 400);
                        move = Vector2.Zero;
                        if( SDraw.IsAlive )
                            SDraw.Abort();
                        if( CCheck.IsAlive )
                            CCheck.Abort();

                        #endregion Clear Data

                        if( Main.menu == 3.2 )
                            dScore += "\nHighest Score This Play: " + mScore.ToString("##0.0") + "\nPress Enter to Continue or Back to Exit";
                        else
                            dScore += "\nScore: " + Main.score.ToString("##0.0") + "\nPress Enter to Continue or Back to Exit";
                        death = false;
                    }
                    s.DrawString(font, "Game Over\n\n" + dScore, new Vector2(Main.size.X / 3, Main.size.Y / 2 - 175), Color.Black);

                    #endregion Death
                }
            } catch( NullReferenceException ) { } catch( ArgumentOutOfRangeException ) { }
        }

        #region Jumping

        public static Vector2
            pos = new Vector2(200, 400),
            move = new Vector2(0, 0); // Circle

        public List<Vector2>
            OLDpos = new List<Vector2>(4) { Vector2.Zero, Vector2.Zero };

        /// <summary>
        /// Jump!
        /// </summary>
        private void Jump() {
            if( !Main.pause ) {
                if( Main.menu == 1 || Main.menu == 3 ) {
                    if( Main.bKstate && !Keyboard.GetState().GetPressedKeys().Contains(Keys.Escape) || Main.bMstate ) {
                        Main.bounce.Play();
                        move.Y = -bHeight * 10;
                    }
                    move.Y += ( float )(bGravity * 1.5);
                } else {
                    if( Main.bKstate && !Keyboard.GetState().GetPressedKeys().Contains(Keys.Escape) || Main.bMstate ) {
                        Main.bounce.Play();
                        move.Y = -600;
                    }
                    move.Y += 75;
                }
            }

            #region Height and Width Limits

            if( pos.Y > Main.size.Y ) { // Bottom screen bounce
                Main.bound.Play();
                if( Main.menu == 3 )
                    Main.score /= 1.2;
                else
                    Main.score -= 0.7;
                prefect = false;
                pos.Y = Main.size.Y;
                move.Y = -325f;
            } else if( pos.Y < 0 ) { // Top screen bounce
                Main.bound.Play();
                if( Main.menu == 3 )
                    Main.score /= 1.2;
                else
                    Main.score -= 0.7;
                prefect = false;
                pos.Y = 0;
                move.Y = 100.0f;
            }

            #endregion Height and Width Limits

            #region After Image

            for( int x = OLDpos.Count - 1; x >= 0; x-- ) {
                if( x == 0 )
                    OLDpos[x] = new Vector2(pos.X - (x + 1) * (mObject / 10), pos.Y);
                else
                    OLDpos[x] = new Vector2(pos.X - (x + 1) * (mObject / 10), OLDpos[x - 1].Y);
            }

            #endregion After Image

            pos += move * Main.gTime; // Change location of Circle
        }

        #endregion Jumping

        #region Collision Checking

        private double mScore = 0;

        /// <summary>
        /// Check Collision with the Circle and the Objects
        /// </summary>
        private void Check() {
            Rectangle box, sObject; // Boundaries
            for( int x = Main.sObject.Count - 1; x >= 0; x-- ) { // Check for Collision of Object
                try {
                    // Define Boundaries
                    box = new Rectangle(( int )pos.X, ( int )pos.Y, 75, 75);
                    if( Main.sObject[x].type == 2 )
                        sObject = new Rectangle(( int )Main.sObject[x].pos.X, ( int )Main.sObject[x].pos.Y, 65, 285);
                    else
                        sObject = new Rectangle(( int )Main.sObject[x].pos.X, ( int )Main.sObject[x].pos.Y, 45, 45);

                    if( Main.check ) {
                        if( sObject.Height == 0 || sObject.Width == 0 )
                            Main.sObject.RemoveAt(x); // If the rectangle is broken
                        else if( sObject.Intersects(box) ) {

                            #region Intersection

                            if( !Main.easter ) {
                                if( Main.menu == 1 ) // Beta
                                    Main.menu = 1.2;
                                else if( Main.menu == 2 ) // Normal
                                    Main.menu = 2.2;
                                else if( Main.menu == 3 ) { // Musical
                                    prefect = false;
                                    float temp = 0; // Score to be subtract
                                    if( Main.score > 1000 ) // If Score is higher than 1000 then minus more points
                                        temp = ( float )(Main.score - Main.sObject[x].sMove * (0.8 + (Main.sObject.Count - 1) / 10));
                                    else // Else minuse less
                                        temp = ( float )(Main.score - Main.sObject[x].sMove * 0.8);
                                    Main.score = MathHelper.Max(temp, 0); // Update point
                                    Main.sObject.RemoveAt(x); // Remove the Object
                                    if( Main.score < 5 && rand.Shuffle(101) > 40 ) // 25% living
                                        Main.menu = 3.2; // Death?
                                }
                            } else if( Main.easter ) {
                                if( Main.menu == 1 || Main.menu == 2 )
                                    Main.score++;
                                else if( Main.menu == 3 ) { // Musical
                                    Main.score += Main.sObject[x].sMove / 3; // Add score based on Object speed / 7
                                    if( Main.score > mScore )
                                        mScore = Main.score; // Update High Score
                                }
                                Main.sObject.RemoveAt(x); // Remove the Object
                            }

                            #endregion Intersection
                        } else if( Main.sObject[x].pos.X < -5 ) { // If pass a point

                            #region Remove Object

                            if( !Main.easter ) {
                                if( Main.menu == 3 ) { // For Musical
                                    Main.score += Main.sObject[x].sMove / 5; // Add score based on Object speed / 7
                                    if( Main.score > mScore )
                                        mScore = Main.score; // Update High Score
                                } else {// For Beta and Flappy
                                    Main.score++; // Increase score
                                    if( Main.score > bHscore && Main.menu == 1 )
                                        bHscore = Main.score;
                                    else if( Main.score > nHscore && Main.menu == 2 )
                                        nHscore = Main.score;
                                }
                            } else if( Main.easter ) {
                                if( Main.menu == 3 ) { // For Musical
                                    float temp = 0; // Score to be subtract
                                    if( Main.score > 1000 ) // If Score is higher than 1000 then minus more points
                                        temp = ( float )(Main.score - Main.sObject[x].sMove * (0.8 + (Main.sObject.Count - 1) / 10));
                                    else if( Main.score < 100 )
                                        temp = ( float )(Main.score - Main.sObject[x].sMove * 0.1);
                                    else // Else minuse less
                                        temp = ( float )(Main.score - Main.sObject[x].sMove * 0.8);
                                    Main.score = MathHelper.Max(temp, 0); // Update point
                                    if( Main.score > mScore )
                                        mScore = Main.score; // Update High Score
                                } else {// For Beta and Flappy
                                    Main.score--; // Increase score
                                    if( Main.score <= 0 && rand.Shuffle(101) > 40 ) // 25% living
                                        Main.menu += 0.2; // Death?
                                    if( Main.score < 0 )
                                        Main.score = 0;
                                }
                            }
                            Main.sObject.RemoveAt(x); // Remove the Object

                            #endregion Remove Object
                        }
                    }
                } catch( NullReferenceException ) { }
            }
        }

        #endregion Collision Checking

        #region Sleeping

        public int
            sleepTime, // Sleep Time
            sObject, // Type of Object
            multi, // Multiple
            percent, // Chance of Speed
            speedy; // * Speed

        public static int mObject; // Object Speed

        /// <summary>
        /// Add the Sprite Object to the sObject in the Main
        /// </summary>
        public void sAdd() {
            List<float> freq;
            int mAmount = 0;
            // Lowest because this is not as important as the others. Beside, this does not require much calculation
            SDraw.Priority = ThreadPriority.Lowest;
            while( true ) { // Always Run

                #region Music Sleeptime

                if( Main.menu == 3 ) {
                    freq = Music.visual.Frequencies.ToList();
                    // Get value from the frequencies list and then minus it from 2500
                    // The intenser the song (higher freq) the quicker the spawn time
                    sleepTime = 2500 - ( int )freq.ValueSum() * rand.Shuffle(15, 25);
                    if( sleepTime < 250 )
                        sleepTime = 250;
                }

                #endregion Music Sleeptime

                Thread.Sleep(sleepTime);
                if( Main.menu == 1 || Main.menu == 3 ) {

                    #region Beta & Musical

                    if( Main.menu == 1 ) {

                        #region Beta Cal

                        mAmount = rand.Shuffle(1, multi);
                        if( percent != 0 ) {
                            List<int> hundred = Enumerable.Range(0, 99).ToList<int>(); // Create a List from 0 - 99
                            hundred.Shuffle(); // Shuffle the List
                            if( percent != 100 )
                                hundred.RemoveRange(percent - 1, hundred.Count - percent); // Remove elements so only a percent is left

                            // Increase the speed if hundred contains the random number
                            if( hundred.Contains(rand.Shuffle(48, 52)) )
                                speedy = 3; // Three Time Speed
                            else if( hundred.Contains(rand.Shuffle(99)) )
                                speedy = 2; // Two Time Speed
                            else
                                speedy = 1; // No Speed
                        }

                        #endregion Beta Cal
                    } else if( Main.menu == 3 ) {

                        #region Musical Calculation

                        if( rand.Shuffle(50) > 10 )
                            sObject = 3;
                        else
                            sObject = 2;
                        freq = Music.visual.Frequencies.ToList();
                        // Get value from the last few frequencies because speedy object are hard
                        speedy = ( int )(freq[rand.Shuffle(200, 255)] + freq[rand.Shuffle(200, 255)] + freq[rand.Shuffle(200, 255)]) * 2;
                        if( speedy <= 0 )
                            speedy = 1;
                        else if( speedy > 3 )
                            speedy = 3;
                        // Get random value from random freq and randomize a number to calculate the multi spawn
                        mAmount = ( int )(freq[rand.Shuffle(freq.Count - 1)] + freq[rand.Shuffle(freq.Count - 1)] + freq[rand.Shuffle(freq.Count - 1)] + freq[rand.Shuffle(freq.Count - 1)]);
                        multi = mAmount;

                        #endregion Musical Calculation
                    }
                    do { // Alway Add in a Sprite...
                        Sprites sprite = new Sprites(); // Create a New Sprite Class
                        sprite.pos = new Vector2(Main.size.X + rand.Shuffle(50, 100), rand.Shuffle(( int )Main.size.Y)); // Location of Object
                        if( mAmount > 1 )
                            sprite.LoadContent(sObject + mAmount, mObject * speedy); // For Multi so no more than 1 wall spawn at a time
                        else
                            sprite.LoadContent(sObject, mObject * speedy); // Set the Sprite Class
                        Main.sObject.Add(sprite); // Add the new Sprites class to the list
                        mAmount--;
                    } while( mAmount > 0 ); // ...Unil multi is no more

                    #endregion Beta & Musical
                } else if( Main.menu == 2 ) {

                    #region Normal

                    if( nSpawn == 0 ) {

                        #region Wall

                        int pos = rand.Shuffle(-150, 350);
                        Sprites sprite = new Sprites(); // Create a new Sprites class
                        sprite.LoadContent(sObject, mObject); // Set the Sprite Class
                        sprite.pos = new Vector2(Main.size.X + 50, pos);

                        Main.sObject.Add(sprite); // Add the new Sprites class to the list
                        sprite = new Sprites(); // Create a new Sprites class
                        sprite.LoadContent(sObject, mObject); // Set the Sprite Class
                        sprite.pos = new Vector2(Main.size.X + 50, pos + 500 + rand.Shuffle(-5, 80));

                        Main.sObject.Add(sprite); // Add the new Sprites class to the list

                        #endregion Wall
                    } else {

                        #region Balls

                        for( int x = nSpawn; x > 0; x-- ) {
                            Sprites sprite = new Sprites(); // Create a new Sprites class
                            if( x > 1 )
                                sprite.LoadContent(sObject, mObject * 2); // Set the Sprite Class Speed
                            else
                                sprite.LoadContent(sObject, mObject); // Set the Sprite Class
                            sprite.pos = new Vector2(Main.size.X + rand.Shuffle(50, 100), rand.Shuffle(( int )Main.size.Y)); // Location of Object
                            Main.sObject.Add(sprite); // Add the new Sprites class to the list
                        }

                        #endregion Balls
                    }

                    #endregion Normal
                }
            }
        }

        #endregion Sleeping

        #endregion Game Play

        #region Color Properties

        public static Color col = new Color(0, 0, 0), // The World's Color
            sCol = new Color(0, 0, 0); // Inverse

        public static float
            Cr = 0.0f, Cg = 0.0f, Cb = 0.0f,
            Nr = 0.0f, Ng = 0.0f, Nb = 0.0f, // Color change: C = Current N = New
            CchangeR = 0.0f, CchangeG = 0.0f, CchangeB = 0.0f, // Color change amount
            TdividR = 1, TdividG = 1, TdividB = 1; // Color change Speed

        /// <summary>
        /// Change the world Color
        /// </summary>
        public void cColor() {

            #region Change Red

            if( Nr > (Cr - 2) && Nr < (Cr + 2) || Nr == Cr ) {
                Cr = rand.Shuffle(0, 226); // Random Number between the value of 0 and 225
                TdividR = rand.Shuffle(100, 500);
                CchangeR = (Cr - Nr) / TdividR; // Find the difference between the two Hex and divide by the time to change
            } else if( Nr != Cr ) {
                Nr += CchangeR; // Decrease New Red by the Change Number
            }

            #endregion Change Red

            #region Change Blue

            if( Nb > (Cb - 2) && Nb < (Cb + 2) || Nb == Cb ) {
                Cb = rand.Shuffle(0, 226); // Random Number between the value of 0 and 225
                TdividB = rand.Shuffle(100, 500);
                CchangeB = (Cb - Nb) / TdividB; // Find the difference between the two Hex and divide by the time to change
            } else if( Nr != Cb ) {
                Nb += CchangeB; // Decrease New Blue by the Change Number
            }

            #endregion Change Blue

            #region Change Green

            // Change the Green Color Hex
            if( Ng > (Cg - 2) && Ng < (Cg + 2) || Ng == Cg ) {
                Cg = rand.Shuffle(0, 226); // Random Number between the value of 0 and 225
                TdividG = rand.Shuffle(100, 500);
                CchangeG = (Cg - Ng) / TdividG; // Find the difference between the two Hex and divide by the time to change
            } else if( Ng != Cg ) {
                Ng += CchangeG; // Decrease New Green by the Change Number
            }

            #endregion Change Green

            col = new Color(( int )Nr, ( int )Ng, ( int )Nb, rand.Shuffle(2, 11)); // Main Color
            sCol = new Color(( int )Math.Abs(Nr - 225), ( int )Math.Abs(Ng - 225), ( int )Math.Abs(Nb - 225)); // Secondary
        }

        #endregion Color Properties
    }
}