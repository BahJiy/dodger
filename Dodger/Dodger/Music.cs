using System.Collections.Generic;
using System.Reflection;
using System.IO;

// Custom Library
using Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Dodger {

    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class Music {

        /// <summary>
        /// Hold the Song Data (Including Picture)
        /// </summary>
        public class SongsList {

            public Song sSong { get; set; }

            public Texture2D sPict { get; set; }
        }

        #region Variables

        public static List<SongsList> songs; // List of All Song
        public static int index = 0; // Index of Current Song

        public int
            width = 0, // Width of Visual
            height = 0; // Height

        private Vector2 vLoc; // Location

        public static bool
            play = true, // Allow playing of Song
            start = true; // Game just Started?

        public static VisualizationData visual; // Visual Data

        #endregion Variables

        /// <summary>
        ///  Play Music and Visualize
        /// </summary>
        public Music() {
            songs = new List<SongsList>(); // New List
            vLoc = Vector2.Zero; // Location
            visual = new VisualizationData(); // New Visual Data
            MediaPlayer.IsVisualizationEnabled = true; // Allow Visualization
        }

        /// <summary>
        /// Controls the Music Player
        /// </summary>
        public void MMusic(GraphicsDevice g) {

            #region Music Control

            if( Main.pause && Main.menu == 3.0 ) { } else
                MediaPlayer.GetVisualizationData(visual); // Get New Data
            if( Main.menu != 3.1 && MediaPlayer.State == MediaState.Stopped && play ) {
                index++; // Go to next song
            }
            if( Main.bKstate && Main.menu != 3 && Main.menu != .1 ) {
                if( Keyboard.GetState().IsKeyDown(Keys.Z) ) {
                    index--; // Go to Previous Song
                    play = true;
                    MediaPlayer.Stop();
                } else if( Keyboard.GetState().IsKeyDown(Keys.X) ) {
                    play = true; // Replay a Song
                    MediaPlayer.Stop();
                } else if( Keyboard.GetState().IsKeyDown(Keys.C) ) {
                    songs.Shuffle(); // Shuffle Song List
                    play = false; // Pause
                    MediaPlayer.Stop();
                } else if( Keyboard.GetState().IsKeyDown(Keys.V) ) {
                    index++; // Next Song
                    play = true;
                    MediaPlayer.Stop();
                }
            }
            // Restrict Range
            if( index < 0 )
                index = songs.Count - 1;
            else if( index > songs.Count - 1 )
                index = 0;
            if (MediaPlayer.State == MediaState.Stopped && play)
            {
                MediaPlayer.Play(songs[index].sSong); // Play Song
            }

            #endregion Music Control
        }

        /// <summary>
        /// Draw the Visual Data
        /// </summary>
        /// <param name="viewport">Size of the Game</param>
        /// <param name="s">SpriteBatch</param>
        /// <param name="sText">Texture to Use</param>
        /// <param name="Location">Location of Visual</param>
        public void Visualize( Viewport viewport, SpriteBatch s, Texture2D sText, Vector2 Location ) {
            vLoc -= (vLoc - Location) * Main.gTime; // Location of Visualizer
            for( int f = 0; f < visual.Frequencies.Count; f++ ) { // Draw the image for each frequencies
                // The visualizer is different for Musical Option Screen
                if( Main.menu == 3.1 ) { // For Musical
                    vLoc.Y = viewport.Height * f / visual.Frequencies.Count - 20; // Divide the Frequencies Evenly
                    height = 20; // Height of each
                    width = ( int )((visual.Frequencies[f] * viewport.Width / 2)); // Width
                    
                    s.Draw(sText, new Rectangle(( int )vLoc.X, ( int )vLoc.Y, -height, -width), null, new Color(GamePlay.col.R, GamePlay.col.G, GamePlay.col.B, 50), MathHelper.ToRadians(90), new Vector2(65 / 2, 285 / 2), SpriteEffects.None, 0); // Right Side              
                    s.Draw(sText, new Rectangle(( int )vLoc.X, ( int )vLoc.Y + 20, height, width), null, GamePlay.sCol, MathHelper.PiOver2, new Vector2(65 / 2, 285 / 2), SpriteEffects.None, 0); // Left Side

                } else { // For everything else
                    vLoc.X = viewport.Width * f / visual.Frequencies.Count - 20;
                    width = 20;
                    height = ( int )((visual.Frequencies[f] * viewport.Height / 2));

                    s.Draw(sText, new Rectangle(( int )vLoc.X, ( int )vLoc.Y, -width, -height), null, new Color(GamePlay.sCol.R, GamePlay.sCol.G, GamePlay.sCol.B, 50), MathHelper.PiOver4, new Vector2(65 / 2, 285 / 2), SpriteEffects.None, 0); // Top
                    s.Draw(sText, new Rectangle(( int )vLoc.X, ( int )vLoc.Y, -width, -height), null, new Color(GamePlay.col.R, GamePlay.col.G, GamePlay.col.B)); // Special Bottom
                }
            }
        }
    }
}