﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace Dodger {
    public partial class Unused : Form {
        public Unused() {
            InitializeComponent();

            foreach( string s in Directory.GetFiles(Main.cDir + "\\Music\\Unused Files") )
                Files.Items.Add(Path.GetFileName(s)); // Manual Addition
        }

        private void Act_Click(object sender, EventArgs e) {
            if( Files.SelectedItems.Count - 1 >= 0 ) { // If User wishes to save files
                FolderBrowserDialog sLoc = new FolderBrowserDialog(); // Open browse folders
                string temp = "";// Hold Path
                if( sLoc.ShowDialog() == DialogResult.OK ) {
                    temp = sLoc.SelectedPath; // Get Path
                    foreach( string s in Files.SelectedItems )
                        File.Move(Main.cDir + "\\Music\\Unused Files\\" + s, temp + "\\" + s); // Move the files
                } else
                    return;
            }
            Directory.Delete(Main.cDir + "\\Music\\Unused Files", true); // Remove Extra Files
            Application.Exit();
        }

        private void Nothing_Click(object sender, EventArgs e) {
            Application.Exit();
        }

        private void Files_SelectedIndexChanged(object sender, EventArgs e) {

        }
    }
}
