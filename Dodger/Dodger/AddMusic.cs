﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace Dodger
{
    public partial class AddMusic : Form
    {
        class ABatch {
            public string path {get; set;}
            public string name {get;set;}
            public string pict {get; set;}
        }

        // 0 = Add Song And Pictures
        // 1 = Add Pictures Only
        int mode = 0, 
            // 0 = All
            // 1 = With Pictures
            // 2 = Without Pictures
            lists = 0;
        List<string>
            all = new List<string>(),
            wPic = new List<string>(),
            oPic = new List<string>();
        List<ABatch> bList = new List<ABatch>();
        bool changed = false;

        /// <summary>
        /// Set up objects
        /// All Song name to ComboBox
        /// </summary>
        public AddMusic()
        {
            InitializeComponent();
            foreach (Music.SongsList s in Music.songs)
            {
                all.Add(s.sSong.Name); // All Songs
                if (s.sPict == null) // Those who are lonely
                    oPic.Add(s.sSong.Name);
                else if (s.sPict != null) // Those who are Friends
                    wPic.Add(s.sSong.Name);
            }
            SongBox.Items.AddRange(all.ToArray());
        }

        /// <summary>
        /// Choose a Song File
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BrowseMusic_Click(object sender, EventArgs e)
        {
            OpenFileDialog musicOpen = new OpenFileDialog();
            musicOpen.Filter = "Music File(*.wav;*.mp3)|*.wav;*mp3";
            musicOpen.RestoreDirectory = true;
            if( musicOpen.ShowDialog() == DialogResult.OK ) {
                MusicLocation.Text = musicOpen.FileName;
                SongName.Text = musicOpen.SafeFileName;
            }
            
        }

        /// <summary>
        /// Choose a Picture File
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BrowsePicture_Click(object sender, EventArgs e)
        {
            if (MusicLocation.Text.Count() > 2)
            {
                OpenFileDialog pictureOpen = new OpenFileDialog();
                pictureOpen.Filter = "Image Files(*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF";
                pictureOpen.RestoreDirectory = true;
                if (pictureOpen.ShowDialog() == DialogResult.OK) 
                    PictureLocation.Text = pictureOpen.FileName;
            }
        }

        /// <summary>
        /// Add the current info to the batch list and start
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Add_Click(object sender, EventArgs e)
        {
            if (MusicLocation.Text.Count() > 2 && mode == 0)
            {
                bList.Add(new ABatch {path = "Song:" + MusicLocation.Text, name = SongName.Text, pict = PictureLocation.Text});
            } else if (SongBox.SelectedItem != null && mode == 1) {
                OpenFileDialog oFile = new OpenFileDialog();
                oFile.Filter = "Image Files(*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF";
                oFile.RestoreDirectory =true;
                if (oFile.ShowDialog() == DialogResult.OK){
                    bList.Add(new ABatch{path = oFile.FileName, pict = SongBox.SelectedItem.ToString()});
                }
            }
            this.Close(); // Close the form
            Main.pause = false; // Allow game control
            Main.Exiting = true; // Stop convertion to add new music
            while( Main.cSong.IsAlive ) { } // Wait for convertion to finish
            RunTime(); // Start Batch
        }

        /// <summary>
        /// Copy files accordingly 
        /// </summary>
        private void RunTime() {
            foreach(ABatch s in bList) {
                if (s.name != null) {
                    if (!s.name.Contains(".wav") && !s.name.Contains(".mp3"))
                        s.name += ".mp3";
                    Main.sConvert = "Copying: " + s.name;
                    File.Copy(s.path.Remove(0, 5), Main.cDir + "\\Music\\" + s.name, true);
                    Main.sConvert = "Copying: " + s.name + "'s Picture";
                    File.Copy(s.pict, Main.cDir + "\\Music\\Music Media\\" + s.name.Remove(s.name.IndexOf(".")), true);
                } else if (s.name == null) {
                    Main.sConvert = "Copying Picture for: " + s.name;
                   File.Copy(s.path, Main.cDir + "\\Music\\Music Media\\" + s.pict, true);
                }
            }
            Main.sConvert = "";
            Main.Exiting = false; // Allow Convertion
        }

        /// <summary>
        /// Nothing to see here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Cancel_Click(object sender, EventArgs e)
        {
            Main.pause = false;
            this.Close();
        }

        /// <summary>
        /// Switch Mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Switch_Click(object sender, EventArgs e) {
            if (mode == 1) { // Switch to Song and Picture
                Switch.Text = "Song and Picture";
                Add.Text = "Add Song";
                mode = 0;

                SongLabel.Text = "Type in the Song Name";
                MusicLabel.Text = "Music Location";
                // Show and Enable
                MusicLocation.Show();
                MusicLocation.Enabled = true;
                BrowseMusic.Show();
                BrowseMusic.Enabled = true;
                SongName.Show();
                SongName.Enabled = true;
                PictureLabel.Show();
                PictureLabel.Enabled = true;
                PictureLocation.Show();
                PictureLocation.Enabled = true;
                BrowsePicture.Show();
                BrowsePicture.Enabled = true;

                // Hide and Disable
                SongBox.Hide();
                SongBox.Enabled = false;
                SwitchList.Hide();
                SwitchList.Enabled = false;
            } else if (mode == 0) { // Switch to Picture ONLY
                Switch.Text = "Add Pictures";
                Add.Text = "Add Pict";
                mode = 1;

                SongLabel.Text = "Chose a Song";
                MusicLabel.Text = "Filter Song List";

                // Show and Enable
                SongBox.Show();
                SongBox.Enabled = true;
                SwitchList.Show();
                SwitchList.Enabled = true;

                // Hide and Disable
                MusicLocation.Hide();
                MusicLocation.Enabled = false;
                BrowseMusic.Hide();
                BrowseMusic.Enabled = false;
                SongName.Hide();
                SongName.Enabled = false;
                PictureLabel.Hide();
                PictureLabel.Enabled = false;
                PictureLocation.Hide();
                PictureLocation.Enabled = false;
                BrowsePicture.Hide();
                BrowsePicture.Enabled = false;
            }
        }

        /// <summary>
        ///  Switch song list in Picture Mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SwitchList_Click(object sender, EventArgs e) {
            if (lists == 0) {
                lists = 1;
                SwitchList.Text = "All Songs";
                SongBox.Items.Clear();
                SongBox.Items.AddRange(all.ToArray());
                SongBox.SelectedIndex = 0;
            } else if (lists == 1) {
                lists = 2;
                SwitchList.Text = "All Songs With Pictures";
                SongBox.Items.Clear();
                SongBox.Items.AddRange(wPic.ToArray());
                SongBox.SelectedIndex = 0;
            } else if (lists == 2) {
                lists = 0;
                SwitchList.Text = "All Songs Without Pictures";
                SongBox.Items.Clear();
                SongBox.Items.AddRange(oPic.ToArray());
                SongBox.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// Add to Batch List
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Batch_Click(object sender, EventArgs e)
        {
            if (MusicLocation.Text.Count() > 2 && mode == 0)
            {
                bList.Add(new ABatch {path = "Song:" + MusicLocation.Text, name = SongName.Text, pict = PictureLocation.Text});
            } else if (SongBox.SelectedItem != null && mode == 1) {
                OpenFileDialog ofile = new OpenFileDialog();
                ofile.RestoreDirectory =true;
                if (ofile.ShowDialog() == DialogResult.OK){
                    bList.Add(new ABatch{path = ofile.FileName, pict = SongBox.SelectedItem.ToString()});
                }
            }
            BatchList.Items.Clear();
            BatchList.Items.AddRange(bList.ToArray());
        }    
    }
}
