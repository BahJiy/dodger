﻿namespace Dodger {
    partial class Unused {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.Files = new System.Windows.Forms.ListBox();
            this.Act = new System.Windows.Forms.Button();
            this.Nothing = new System.Windows.Forms.Button();
            this.Text = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // Files
            // 
            this.Files.BackColor = System.Drawing.SystemColors.Control;
            this.Files.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Files.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Files.FormattingEnabled = true;
            this.Files.Location = new System.Drawing.Point(12, 105);
            this.Files.Name = "Files";
            this.Files.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.Files.Size = new System.Drawing.Size(153, 52);
            this.Files.Sorted = true;
            this.Files.TabIndex = 0;
            this.Files.SelectedIndexChanged += new System.EventHandler(this.Files_SelectedIndexChanged);
            // 
            // Act
            // 
            this.Act.Location = new System.Drawing.Point(171, 105);
            this.Act.Name = "Act";
            this.Act.Size = new System.Drawing.Size(75, 23);
            this.Act.TabIndex = 1;
            this.Act.Text = "Act";
            this.Act.UseVisualStyleBackColor = true;
            this.Act.Click += new System.EventHandler(this.Act_Click);
            // 
            // Nothing
            // 
            this.Nothing.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Nothing.Location = new System.Drawing.Point(171, 138);
            this.Nothing.Name = "Nothing";
            this.Nothing.Size = new System.Drawing.Size(75, 23);
            this.Nothing.TabIndex = 2;
            this.Nothing.Text = "Nothing";
            this.Nothing.UseVisualStyleBackColor = true;
            this.Nothing.Click += new System.EventHandler(this.Nothing_Click);
            // 
            // Text
            // 
            this.Text.BackColor = System.Drawing.SystemColors.Control;
            this.Text.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Text.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.Text.Location = new System.Drawing.Point(12, 12);
            this.Text.Multiline = true;
            this.Text.Name = "Text";
            this.Text.ReadOnly = true;
            this.Text.Size = new System.Drawing.Size(234, 87);
            this.Text.TabIndex = 4;
            this.Text.Text = "Dodger.exe have found unused file(s) in it\'s Music folder. To insure speed, Dodge" +
                "r.exe have moved it to a a new folder.\r\nPlease chose the file you wish to save a" +
                "nd click \"Act.\"";
            // 
            // Unused
            // 
            this.AcceptButton = this.Act;
            this.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Nothing;
            this.ClientSize = new System.Drawing.Size(258, 171);
            this.Controls.Add(this.Text);
            this.Controls.Add(this.Nothing);
            this.Controls.Add(this.Act);
            this.Controls.Add(this.Files);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Unused";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox Files;
        private System.Windows.Forms.Button Act;
        private System.Windows.Forms.Button Nothing;
        private System.Windows.Forms.TextBox Text;


    }
}