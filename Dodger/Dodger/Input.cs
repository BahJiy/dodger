using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

// Custom Library
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Input {

    public static class Input {
        private static Keys[] oKey = new Keys[1]; // Previous key
        private static MouseState oMouse; // Previous mouse state
        private static int oscroll = 0; // Previous scroll value

        /// <summary>
        /// Check if keys are being held
        /// </summary>
        /// <param name="key">Keyboard state</param>
        /// <returns>True if key is not held. False if key is being held or no key was pressed.</returns>
        public static bool CheckPress( this KeyboardState key ) {
            bool kPress = false;
            if( key.GetPressedKeys().Length > 0 ) { // A key was pressed
                Keys nKey = key.GetPressedKeys()[0];
                try {
                    if( nKey.Equals(oKey[0]) ) {
                        kPress = false; // Key is being held
                    } else {
                        oKey = key.GetPressedKeys(); // Update KeyState
                        kPress = true; // Key is NOT being held
                    }
                } catch( IndexOutOfRangeException ) {
                    oKey = key.GetPressedKeys();
                    kPress = true;
                }
            }
            // A key was NOT pressed
            oKey = key.GetPressedKeys(); // Update KeyState
            return kPress; // No key was press
        }

        /// <summary>
        /// Check if buttons are being held
        /// </summary>
        /// <param name="mouse">Mouse state</param>
        /// <returns>True if button is not held. False if button is being held or no button was pressed</returns>
        public static bool CheckPress( this MouseState mouse ) {
            bool mPress = false;
            // Check the left button
            if( oMouse.LeftButton == ButtonState.Pressed && mouse.LeftButton == ButtonState.Pressed ) {
                mPress = false; // If player is holding the button
            } else if( oMouse.LeftButton == ButtonState.Released && mouse.LeftButton == ButtonState.Pressed ) {
                oMouse = mouse; // Set old mouse value to the new
                mPress = true; // If player is NOT holding the button
            }
            // Check the right button
            if( oMouse.RightButton == ButtonState.Pressed && mouse.RightButton == ButtonState.Pressed ) {
                mPress = false; // If player is holding the button
            } else if( oMouse.RightButton == ButtonState.Released && mouse.RightButton == ButtonState.Pressed ) {
                oMouse = mouse;// Set old mouse value to the new
                mPress = true;// If player is NOT holding the button
            }
            // No key was press
            oMouse = mouse;
            return mPress;
        }

        /// <summary>
        /// Check mouse scroll wheel of change
        /// </summary>
        /// <param name="scroll">Current scroll amount</param>
        /// <returns>1 if player have scrolled up. -1 if player have scrolled down. 0 if player did not scroll</returns>
        public static int CheckScroll( this MouseState mouse ) {
            int scroll = mouse.ScrollWheelValue, value = 0;

            if( scroll > oscroll ) { // If scroll up
                oscroll = scroll; // Set old scroll value to the new
                value = 1;
            } else if( scroll < oscroll ) { // If scroll down
                oscroll = scroll; // Set old scroll value to the new
                value = -1;
            }
            oscroll = scroll;
            return value; // Player did not scroll
        }

        /// <summary>
        /// Get the keyboard input and add it to the current word
        /// </summary>
        /// <param name="keys">The Key Class</param>
        /// <param name="word">The string to edit</param>
        /// <returns></returns>
        public static string Keystroke( this KeyboardState keys, string word, bool bKstate ) {
            if( bKstate && Keyboard.GetState().GetPressedKeys().Length > 0 ) { // Make sure the user is NOT holding
                foreach( Keys k in Keyboard.GetState().GetPressedKeys() ) {

                    #region Normal Letters

                    if( k.ToString().Length == 1 ) { // If it is a single letter key
                        if( !Keyboard.GetState().GetPressedKeys().Contains<Keys>(Keys.RightShift) && !Keyboard.GetState().GetPressedKeys().Contains<Keys>(Keys.LeftShift) ) { // If shift is NOT pressed
                            word += k.ToString().ToLower();
                        } else if( Keyboard.GetState().GetPressedKeys().Contains<Keys>(Keys.RightShift) || Keyboard.GetState().GetPressedKeys().Contains<Keys>(Keys.LeftShift) ) { // If shift IS pressed
                            word += k.ToString();
                        }

                    #endregion Normal Letters

                        #region Backspace
                    } else if( k.ToString().Contains("F") ) { // If a Function Key was pressed
                    } else if( k.Equals(Keys.Back) ) { // Erase letters
                        try { // Just in case user press Back when there are no text
                            word = word.Remove(word.Length - 1); // Remove the last letter
                        } catch( ArgumentOutOfRangeException ) { } // Just case something happens

                        #endregion Backspace

                        #region Symbols
                    } else if( Keyboard.GetState().GetPressedKeys().Contains<Keys>(Keys.RightShift) || Keyboard.GetState().GetPressedKeys().Contains<Keys>(Keys.LeftShift) ) { // Shift symbol

                        #region Number Symbol (0 - 9)

                        if( k.Equals(Keys.D0) )
                            word += ")";
                        else if( k.Equals(Keys.D1) )
                            word += "!";
                        else if( k.Equals(Keys.D2) )
                            word += "@";
                        else if( k.Equals(Keys.D3) )
                            word += "#";
                        else if( k.Equals(Keys.D4) )
                            word += "$";
                        else if( k.Equals(Keys.D5) )
                            word += "%";
                        else if( k.Equals(Keys.D6) )
                            word += "^";
                        else if( k.Equals(Keys.D7) )
                            word += "&";
                        else if( k.Equals(Keys.D8) )
                            word += "*";
                        else if( k.Equals(Keys.D9) )
                            word += "(";

                        #endregion Number Symbol (0 - 9)

                        #region Other Symbols

                        // "Oem" Symbols
                        else if( k.Equals(Keys.OemPipe) )
                            word += "|";
                        else if( k.Equals(Keys.OemCloseBrackets) )
                            word += "}";
                        else if( k.Equals(Keys.OemComma) )
                            word += "<";
                        else if( k.Equals(Keys.OemMinus) )
                            word += "_";
                        else if( k.Equals(Keys.OemOpenBrackets) )
                            word += "{";
                        else if( k.Equals(Keys.OemPeriod) )
                            word += ">";
                        else if( k.Equals(Keys.OemPlus) )
                            word += "+";
                        else if( k.Equals(Keys.OemQuotes) )
                            word += "\"";
                        else if( k.Equals(Keys.OemSemicolon) )
                            word += ":";
                        else if( k.Equals(Keys.OemTilde) )
                            word += "~";
                        else if( k.Equals(Keys.OemQuestion) )
                            word += "?";
                    } else if( k.ToString().Contains("Oem") ) {
                        if( k.Equals(Keys.OemPipe) )
                            word += "\\";
                        else if( k.Equals(Keys.OemCloseBrackets) )
                            word += "]";
                        else if( k.Equals(Keys.OemComma) )
                            word += ",";
                        else if( k.Equals(Keys.OemMinus) )
                            word += "-";
                        else if( k.Equals(Keys.OemOpenBrackets) )
                            word += "[";
                        else if( k.Equals(Keys.OemPeriod) )
                            word += ".";
                        else if( k.Equals(Keys.OemPlus) )
                            word += "=";
                        else if( k.Equals(Keys.OemQuotes) )
                            word += "'";
                        else if( k.Equals(Keys.OemSemicolon) )
                            word += ";";
                        else if( k.Equals(Keys.OemTilde) )
                            word += "`";
                        else if( k.Equals(Keys.OemQuestion) )
                            word += "/";
                    } else if( k.Equals(Keys.Divide) )
                        word += "/";
                    else if( k.Equals(Keys.Multiply) )
                        word += "*";
                    else if( k.Equals(Keys.Subtract) )
                        word += "-";
                    else if( k.Equals(Keys.Decimal) )
                        word += ".";
                    else if( k.Equals(Keys.Add) )
                        word += "+";

                        #endregion Other Symbols

                        #endregion Symbols

                    #region Basic Other Key

                    else if( k.Equals(Keys.Enter) )
                        word += "\n";
                    else if( k.Equals(Keys.Space) )
                        word += " ";
                    else if( k.Equals(Keys.Tab) )
                        word += "    ";

                    #endregion Basic Other Key

                    #region Parsing Numbers

                    else {
                        try { // Get rid of all letters and leave only numbers
                            word += Regex.Replace(k.ToString(), "[^0-9]", "");
                        } catch( FormatException ) { // Just in case there are some weird character pressed
                            return word;
                        }
                    }

                    #endregion Parsing Numbers

                    return word;
                }
            }
            return word;
        }

        /// <summary>
        /// Return the X and Y of the Mouse
        /// </summary>
        /// <param name="mouse"></param>
        /// <returns></returns>
        public static Vector2 Position( this MouseState mouse ) {
            return new Vector2(mouse.X, mouse.Y);
        }

        /// <summary>
        /// See if a List contains any elements from another List
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">Original List</param>
        /// <param name="cList">List to Compare</param>
        /// <returns></returns>
        public static bool ContainList<T>( this List<T> list, List<T> cList ) {
            foreach( T o in cList ) {
                if( list.Contains(o) )
                    return true;
            }

            return false;
        }

        /// <summary>
        /// See if a List contains ALL elements from another List
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">Original List</param>
        /// <param name="cList">List to Compare</param>
        /// <returns></returns>
        public static bool ContainListAll<T>( this List<T> list, List<T> cList ) {
            foreach( T o in cList ) {
                if( !list.Contains(o) )
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Get a Random Number based on the Fisher-Yates Randomizer
        /// </summary>
        /// <param name="rand"></param>
        /// <param name="x">Ending number</param>
        /// <returns>Random Number</returns>
        public static int Shuffle( this Random rand, int x ) {
            List<int> number = Enumerable.Range(0, x + 1).ToList();
            int n = number.Count;
            while( n > 1 ) {
                n--;
                int k = rand.Next(n + 1);
                int value = number[k];
                number[k] = number[n];
                number[n] = value;
            }
            return number[rand.Next(number.Count - 1)];
        }

        /// <summary>
        /// Get a Random Number based on the Fisher-Yates Randomizer
        /// </summary>
        /// <param name="rand"></param>
        /// <param name="x">Starting number</param>
        /// <param name="y">Ending number</param>
        /// <returns>Random number</returns>
        public static int Shuffle( this Random rand, int x, int y ) {
            try {
                List<int> number = Enumerable.Range(x, y - x + 1).ToList();
                int n = number.Count;
                while( n > 1 ) {
                    n--;
                    int k = rand.Next(n + 1);
                    int value = number[k];
                    number[k] = number[n];
                    number[n] = value;
                }

                return number[rand.Next(number.Count - 1)];
            } catch( ArgumentOutOfRangeException ) {
                return 0;
            }
        }

        /// <summary>
        /// Shuffle the List using Fisher-Yates Randomizer
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">List to shuffle</param>
        public static void Shuffle<T>( this List<T> list ) {
            Random rand = new Random();
            int n = list.Count;
            while( n > 1 ) {
                n--;
                int k = rand.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        /// <summary>
        /// Get the sum of all the variable in a list up to a point
        /// </summary>
        /// <typeparam name="?">Generic</typeparam>
        /// <param name="list">List to add</param>
        /// <param name="x">Index of last value to add</param>
        /// <returns>Sum from the first vaule to x</returns>
        public static double ValueSum<T>( this List<T> list, int x ) {
            double sum = 0;
            try {
                for( int z = 0; z <= x; z++ ) {
                    sum += Convert.ToDouble(list[z]);
                }
            } catch( IndexOutOfRangeException ) {
                return sum;
            }
            return sum;
        }

        /// <summary>
        /// Get the sum of all the variable in a list up to a point
        /// </summary>
        /// <typeparam name="?">Generic</typeparam>
        /// <param name="list">List to add</param>
        /// <param name="x">Index of first value to add</param>
        /// /// <param name="y">Index of last value to add</param>
        /// <returns>Sum from x to y</returns>
        public static double ValueSum<T>( this List<T> list, int x, int y ) {
            double sum = 0;
            try {
                for( int z = x; z <= y; z++ ) {
                    sum += Convert.ToDouble(list[z]);
                }
            } catch( IndexOutOfRangeException ) {
                return sum;
            }
            return sum;
        }

        /// <summary>
        /// The sum of the whole list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">List to add</param>
        /// <returns>Sum of List</returns>
        public static double ValueSum<T>( this List<T> list ) {
            double sum = 0;
            try {
                for( int z = 0; z <= list.Count - 1; z++ ) {
                    sum += Convert.ToDouble(list[z]);
                }
            } catch( IndexOutOfRangeException ) {
                return sum;
            }
            return sum;
        }
    }
}